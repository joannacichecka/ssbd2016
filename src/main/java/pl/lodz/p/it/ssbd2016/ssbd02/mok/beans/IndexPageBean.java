package pl.lodz.p.it.ssbd2016.ssbd02.mok.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.EmailException;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.ShortConverter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Ziarno widoku strony głównej.
 * @author mgrabski
 */

@Named("indexPageBean")
@RequestScoped
public class IndexPageBean {

    /**
     * Domyślny konstruktor bezparametrowy inicjalizujący zmienną propertiesDict.
     */
	IndexPageBean() {
        propertiesDict = new Text();
    }

    @Inject
    private AccountSession accountSession;

    private Account currentAccount;
    private String username;
    private String password;
    private String errorMessage;
    private boolean error = false;
    private Text propertiesDict;
    
    /**
     * Inicjalizacja modelu - metoda pobiera konto aktualnie zalogowanego użytkownika.
     */
    @PostConstruct
    public void init() {
        currentAccount = accountSession.getCurrentAccount();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    private void setError(boolean error) {
        this.error = error;
    }

    public Account getCurrentAccount() {
        return currentAccount;
    }

    public void setCurrentAccount(Account currentAccount) {
        this.currentAccount = currentAccount;
    }

    /**
     * Metoda wywołująca wylogowanie użytkownika.
     */
    public void logout() {
        try {
            accountSession.logout();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda umożliwiająca zalogowanie użytkownika.
     */
    public void login() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        boolean isAccount = true;

        try {
            currentAccount = accountSession.getAccountByLogin(username);
        } catch (AccountException ex) {
            isAccount = false;
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(IndexPageBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (isAccount) {
            try {
                request.login(username, password);
                externalContext.getSessionMap().put("account", currentAccount);
                addOperationMessage(false, "accountlogged");
                currentAccount.setFailedAttempts((short) 0);
                currentAccount.setSuccessfulLogin(new Date());
            } catch (ServletException ex) {
                addOperationMessage(true, "accountnotlogged");
                checkLoginAttempts();
                setError(true);
                setErrorMessage(ex.getMessage());
                Logger.getLogger(IndexPageBean.class.getName()).log(Level.SEVERE, null, ex);
                addMessage(true, ex.getMessage());
            }

            editLoginStats();

            if (!isError()) {
                try {
                    externalContext.redirect(externalContext.getRequestContextPath() + "/index.xhtml");
                } catch (IOException ex) {
                    setError(true);
                    setErrorMessage(ex.getMessage());
                    Logger.getLogger(IndexPageBean.class.getName()).log(Level.SEVERE, null, ex);
                    addMessage(true, ex.getMessage());
                }

            }

        }
    }

    private void editLoginStats() {
        try {

            accountSession.editLoginStats(currentAccount);

        } catch (AccountException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(IndexPageBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        } catch (EmailException e) {
            setError(true);
            setErrorMessage(e.getMessage());
            Logger.getLogger(IndexPageBean.class.getName()).log(Level.SEVERE, null, e);
            addMessage(true, e.getMessage());
        }
    }

    /**
     * Metoda sprawdzająca liczbę nieudanych logowań pod rząd i blokująca konto, 
     * jeśli jest to trzecia nieudana próba.
     */
    private void checkLoginAttempts() {
        Short attempts = currentAccount.getFailedAttempts() == null ? 0 : currentAccount.getFailedAttempts();
        ++attempts;
        currentAccount.setFailedAttempts(attempts);
        currentAccount.setFailedLogin(new Date());
        if (attempts >= 3) {
            currentAccount.setActive(false);
            addOperationMessage(true, "accountblocked");
        } else {
            addAttemptsMessage(true, attempts);
        }
    }

    /**
     * Metoda sprawdzająca, czy osoba jest uwierzytelniona.
     * @return true - jest uwierzytelniona, false - nie jest uwierzytelniona.
     */
    public boolean isUserLogged() {
        Principal principal = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
        if(principal!=null) return true;
        return false;
    }

    /**
     * Metoda sprawdzająca, czy użytkownik jest w roli.
     * Wykorzystuje zapisane w roles.properties pary klucz-nazwa roli.
     * @param role klucz z roles.properties
     * @return true - użytkownik jest w roli, false - użytkownik nie jest w roli
     */
    private boolean isInRole(String role) {
        String roleName = ResourceBundle.getBundle("roles").getString(role);
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole(roleName);
    }

    /**
     * Metoda sprawdza, czy użytkownik jest administratorem.
     * @return true - jest administratorem, false - nie jest administratorem.
     */
    public boolean isAdmin() {
        return isInRole("admin");
    }

    /**
     * Metoda sprawdza, czy użytkownik jest moderatorem.
     * @return true - jest moderatorem, false - nie jest moderatorem.
     */
    public boolean isModerator() {
        return isInRole("moderator");
    }

    /**
     * Metoda sprawdza, czy użytkownik jest pracodawcą.
     * @return true - jest pracodawcą, false - nie jest pracodawcą.
     */
    public boolean isEmployer() {
        return isInRole("employer");
    }

    /**
     * Metoda sprawdza, czy użytkownik jest pracownikiem.
     * @return true - jest pracownikiem, false - nie jest pracownikiem.
     */
    public boolean isEmployee() {
        return isInRole("employee");
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	/**
	 * Dodanie komunikatu o wyniku operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
	 */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

	/**
	 * Dodanie komunikatu o ilości pozostałych prób zalogowania.
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param attempts Ilość prób
	 */
    private void addAttemptsMessage(boolean isError, short attempts) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        FacesMessage message = new FacesMessage(severity, propertiesDict.getString("attemptsleft"), String.valueOf(3 -
                attempts));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

	/**
	 * Dodanie komunikatu w przypadku niepowodzenia operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Wiadomość ze złapanego wyjątku
	 */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }


}
