/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mteo.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface EmployerFacadeLocal {
    /**
     * Zwraca konto pracodawcy dla podanego loginu
     * @param login Login pracodawcy
     * @return
     * @throws ApplicationException
     */
    Employer findByAccout(String login) throws ApplicationException;
}
