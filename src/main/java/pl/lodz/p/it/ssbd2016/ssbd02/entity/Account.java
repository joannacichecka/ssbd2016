/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.entity;

import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Patryk
 */
@Entity
@Table(name = "account")
@NamedQueries({
        @NamedQuery(name = "Account.findAll", query = "SELECT a FROM Account a"),
        @NamedQuery(name = "Account.findById", query = "SELECT a FROM Account a WHERE a.id = :id"),
        @NamedQuery(name = "Account.findByLogin", query = "SELECT a FROM Account a WHERE a.login = :login"),
        @NamedQuery(name = "Account.findByPassword", query = "SELECT a FROM Account a WHERE a.password = :password"),
        @NamedQuery(name = "Account.findByEmail", query = "SELECT a FROM Account a WHERE a.email = :email"),
        @NamedQuery(name = "Account.findByActive", query = "SELECT a FROM Account a WHERE a.active = :active"),
        @NamedQuery(name = "Account.findBySuccessfulLogin", query = "SELECT a FROM Account a WHERE a.successfulLogin = :successfulLogin"),
        @NamedQuery(name = "Account.findByFailedLogin", query = "SELECT a FROM Account a WHERE a.failedLogin = :failedLogin"),
        @NamedQuery(name = "Account.findByFailedAttempts", query = "SELECT a FROM Account a WHERE a.failedAttempts = :failedAttempts"),
        @NamedQuery(name = "Account.findByVersion", query = "SELECT a FROM Account a WHERE a.version = :version"),
        @NamedQuery(name = "Account.findByProfileType", query = "SELECT a FROM Account a WHERE a.profileType = :profileType")})
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "account_id_seq", sequenceName = "account_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "login", updatable = false)
    private String login;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "password")
    private String password;
    @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 30)
    @Column(name = "email")
    private String email;
    @Column(name = "successful_login")
    @Temporal(TemporalType.TIMESTAMP)
    private Date successfulLogin;
    @Column(name = "failed_login")
    @Temporal(TemporalType.TIMESTAMP)
    private Date failedLogin;
    @Column(name = "failed_attempts")
    private Short failedAttempts;
    @NotNull
    @Column(name = "version")
    @Version
    private int version;
    @Column(name = "profile_type")
    @Enumerated(EnumType.STRING)
    private ProfileType profileType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;
    @OneToOne(cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, mappedBy = "account")
    private Employee employee;
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE}, orphanRemoval = true,
            mappedBy = "accountId")
    private List<AccessLevel> accessLevelList = new ArrayList<>();
    @OneToOne(cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, mappedBy = "account")
    private Employer employer;

    public String profileTypeValue(ProfileType p) {
        return p.toString();
    }

    public Account() {
    }

    public Account(Integer id) {
        this.id = id;
    }

    public Account(boolean createByAdmin) {
        if(createByAdmin) {
            setAllAccessLevels();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getSuccessfulLogin() {
        return successfulLogin;
    }

    public void setSuccessfulLogin(Date successfulLogin) {
        this.successfulLogin = successfulLogin;
    }

    public Date getFailedLogin() {
        return failedLogin;
    }

    public void setFailedLogin(Date failedLogin) {
        this.failedLogin = failedLogin;
    }

    public Short getFailedAttempts() {
        return failedAttempts;
    }

    public void setFailedAttempts(Short failedAttempts) {
        this.failedAttempts = failedAttempts;
    }

    public ProfileType getProfileType() {
        return profileType;
    }

    public void setProfileType(ProfileType profileType) {
        this.profileType = profileType;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<AccessLevel> getAccessLevelList() {
        return accessLevelList;
    }

    public void setAccessLevelList(List<AccessLevel> accessLevelList) {
        this.accessLevelList = accessLevelList;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public boolean isAdmin() {
        for (AccessLevel accessLevel : accessLevelList) {
            if (accessLevel.getLevel() == RoleType.ADMIN) return true;
        }

        return false;
    }

    public void setAdmin(boolean state) {
        boolean actual = isAdmin();

        if (state && !actual) {
            AccessLevel newAccessLevel = new AccessLevel();
            newAccessLevel.setLevel(RoleType.ADMIN);
            newAccessLevel.setActive(true);
            newAccessLevel.setAccountId(this);
            accessLevelList.add(newAccessLevel);
        } else if (!state && actual) {
            for (AccessLevel accessLevel : accessLevelList) {
                if (accessLevel.getLevel() == RoleType.ADMIN) {
                    accessLevelList.remove(accessLevel);
                    break;
                }
            }
        }
    }

    public boolean isModerator() {
        for (AccessLevel accessLevel : accessLevelList) {
            if (accessLevel.getLevel() == RoleType.MODERATOR) return true;
        }

        return false;
    }

    public void setModerator(boolean state) {
        boolean actual = isModerator();

        if (state && !actual) {
            AccessLevel newAccessLevel = new AccessLevel();
            newAccessLevel.setLevel(RoleType.MODERATOR);
            newAccessLevel.setActive(true);
            newAccessLevel.setAccountId(this);
            accessLevelList.add(newAccessLevel);
        } else if (!state && actual) {
            for (AccessLevel accessLevel : accessLevelList) {
                if (accessLevel.getLevel() == RoleType.MODERATOR) {
                    accessLevelList.remove(accessLevel);
                    break;
                }
            }
        }
    }

    public boolean isEmployer() {
        for (AccessLevel accessLevel : accessLevelList) {
            if (accessLevel.getLevel() == RoleType.EMPLOYER) return true;
        }

        return false;
    }

    public void setEmployer(boolean state) {
        boolean actual = isEmployer();

        if (state && !actual) {
            AccessLevel newAccessLevel = new AccessLevel();
            newAccessLevel.setLevel(RoleType.EMPLOYER);
            newAccessLevel.setActive(true);
            newAccessLevel.setAccountId(this);
            accessLevelList.add(newAccessLevel);
        } else if (!state && actual) {
            for (AccessLevel accessLevel : accessLevelList) {
                if (accessLevel.getLevel() == RoleType.EMPLOYER) {
                    accessLevelList.remove(accessLevel);
                    break;
                }
            }
        }
    }

    public boolean isEmployee() {
        for (AccessLevel accessLevel : accessLevelList) {
            if (accessLevel.getLevel() == RoleType.EMPLOYEE) return true;
        }
        return false;
    }

    public void setEmployee(boolean state) {
        boolean actual = isEmployee();

        if (state && !actual) {
            AccessLevel newAccessLevel = new AccessLevel();
            newAccessLevel.setLevel(RoleType.EMPLOYEE);
            newAccessLevel.setActive(true);
            newAccessLevel.setAccountId(this);
            accessLevelList.add(newAccessLevel);
        } else if (!state && actual) {
            for (AccessLevel accessLevel : accessLevelList) {
                if (accessLevel.getLevel() == RoleType.EMPLOYEE) {
                    accessLevelList.remove(accessLevel);
                    break;
                }
            }
        }
    }

    public void setAllAccessLevels(){
        setAdmin(true);
        setModerator(true);
        setEmployer(true);
        setEmployee(true);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Account)) {
            return false;
        }
        Account other = (Account) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.ssbd2016.ssbd02.entity.Account[ id=" + id + " ]";
    }

}
