/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mteo.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Category;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface CategoryFacadeLocal {
    /**
     * Zwraca obiekt klasy Category dla podanej nazwy kategori
     * @param name Nazwa kategori
     * @return Kategoria o podanej nazwie
     * @throws ApplicationException
     */
    Category findByName(String name) throws ApplicationException;

    /**
     * Zwraca listę wszystkich kategorii z bazy danych
     * @return Lista wszystkich ategorii
     * @throws ApplicationException
     */
    List<Category> findAll() throws ApplicationException;

    /**
     * Zwraca listę kategorii dla podanej listy nazw kategorii
     * @param categories Lista nazw kategorii
     * @return Lista kategorii o podanych nazwach
     * @throws ApplicationException
     */
    List<Category> findList(List<String> categories) throws ApplicationException;
}
