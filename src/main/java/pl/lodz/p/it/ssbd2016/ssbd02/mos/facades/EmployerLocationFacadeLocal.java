/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mos.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerLocation;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface EmployerLocationFacadeLocal {

    void create(EmployerLocation employerLocation) throws ApplicationException;

    void edit(EmployerLocation employerLocation) throws ApplicationException;

    void remove(EmployerLocation employerLocation);

    EmployerLocation find(Object id) throws ApplicationException;

    List<EmployerLocation> findAll() throws ApplicationException;

    List<EmployerLocation> findRange(int[] range);

    int count();
    
}
