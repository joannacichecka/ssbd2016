package pl.lodz.p.it.ssbd2016.ssbd02.mpo.endpoints;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Advert;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.AdvertResponse;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AdvertException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;


public interface MPOEndpointLocal {

    /**
     * Szuka ogłoszeń
     * @param phrase fraza
     * @param cityName miasto
     * @return Liste ogłoszen
     * @throws ApplicationException
     */
    List<Advert> searchForAdvert(String phrase, String cityName) throws ApplicationException;

    /**
     * Szuka ogłoszeń
     * @return Liste ogłoszen
     * @throws AdvertException
     */
    List<Advert> getAdverts() throws AdvertException;

    /**
     * Szuka ogłoszenia po id
     * @param id
     * @return Ogloszenie
     * @throws AdvertException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    Advert getAdvert(Integer id) throws AdvertException, IOException, ClassNotFoundException;

    /**
     * Wysyla ogloszenie do bazy
     * @param currentAdvert Ogloszenie na ktor uzielono odpowiedzi
     * @throws ApplicationException
     */
    void sendResponse(Advert currentAdvert) throws ApplicationException;

    /**
     * Pobiera odpowiedzi na ogłoszenie.
     * @param advertId id ogłoszenia.
     * @return
     * @throws ApplicationException
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws IOException
     */
    List<AdvertResponse> getResponses(Integer advertId) throws ApplicationException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, IOException;

    AdvertResponse getResponse(Integer responseId);

    List<City> getCities() throws ApplicationException;

    void editAdvert(Advert advert) throws ApplicationException;

    void setCurrentAdvert(Integer id) throws AdvertException, IOException, ClassNotFoundException;

    Advert getCurrentAdvert();
}
