/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mp.facades;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerLocation;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;

/**
 * Fasada do zarządzania lokalizacjami
 * @author Patryk
 */
@Stateless(name = "EmployerLocationFacadeMP")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class EmployerLocationFacade extends AbstractFacade<EmployerLocation> implements EmployerLocationFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mpPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployerLocationFacade() {
        super(EmployerLocation.class);
    }
    
}
