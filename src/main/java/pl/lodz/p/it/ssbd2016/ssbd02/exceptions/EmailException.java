package pl.lodz.p.it.ssbd2016.ssbd02.exceptions;

import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

/**
 * Created by Joanna Cichecka on 2016-06-16.
 */
public class EmailException extends ApplicationException {
    public static final String WRONG_EMAIL = "EMAIL.WRONG.ADDRESS.EXCEPTION";
    public static final String EMAIL_EXCEPTION = "EMAIL.SENDING.ERROR.EXCEPTION";

    public EmailException() {}
    public EmailException(String message) {
        super(message);
    }
    public EmailException(String message, Throwable cause) {
        super(message, cause);
    }


    public static EmailException createWrongEmailException(Throwable cause) {
        return createException(cause, "EMAIL.WRONG.ADDRESS.EXCEPTION");
    }

    public static EmailException createEmailException(Throwable cause) {
        return createException(cause, "EMAIL.SENDING.ERROR.EXCEPTION");
    }

    private static EmailException createException(Throwable cause, String msg) {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(msg);

        EmailException emailException = new EmailException(message, cause);
        return emailException;
    }
}
