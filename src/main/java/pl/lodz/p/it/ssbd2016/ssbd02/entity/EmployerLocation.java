/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author Patryk
 */
@Entity
@Table(name = "employer_location")
@NamedQueries({
        @NamedQuery(name = "EmployerLocation.findAll", query = "SELECT e FROM EmployerLocation e"),
        @NamedQuery(name = "EmployerLocation.findById", query = "SELECT e FROM EmployerLocation e WHERE e.id = :id"),
        @NamedQuery(name = "EmployerLocation.findByAddress", query = "SELECT e FROM EmployerLocation e WHERE e" +
                ".address = :address")})
public class EmployerLocation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Size(max = 30)
    @Column(name = "address")
    private String address;
    @JoinColumn(name = "city_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private City cityId;
    @JoinColumn(name = "employer_id", referencedColumnName = "id", updatable = false)
    @ManyToOne(optional = false)
    private Employer employerId;

    public EmployerLocation() {
    }

    public EmployerLocation(Employer employer) {
        this.employerId = employer;
        this.cityId = new City();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public City getCityId() {
        return cityId;
    }

    public void setCityId(City cityId) {
        this.cityId = cityId;
    }

    public Employer getEmployerId() {
        return employerId;
    }

    public void setEmployerId(Employer employerId) {
        this.employerId = employerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployerLocation)) {
            return false;
        }
        EmployerLocation other = (EmployerLocation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerLocation[ id=" + id + " ]";
    }

}
