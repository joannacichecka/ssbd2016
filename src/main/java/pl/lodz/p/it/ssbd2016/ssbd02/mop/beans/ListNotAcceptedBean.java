package pl.lodz.p.it.ssbd2016.ssbd02.mop.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerRating;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.RatingException;
import pl.lodz.p.it.ssbd2016.ssbd02.mop.endpoints.MOPEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Joanna Cichecka on 2016-06-10.
 */
/**
 * Zajmuje się listą niezaakcpetowanych opini o pracodawcach.
 */
@Named("listNotAcceptedBean")
@RequestScoped
public class ListNotAcceptedBean {
    @EJB
    MOPEndpointLocal mopEndpoint;
    private DataModel<EmployerRating> ratingDataModel;
    private Text propertiesDict;
    private boolean error = false;
    private String errorMessage;

    /**
     * Funkcja inicjalizująca, pobierająca z bazy listę wszystkich niezaakceptowanych
     * opini o pracodawcy
     */
    @PostConstruct
    public void initModel() {
        try {
            ratingDataModel = new ListDataModel<>(mopEndpoint.getNotAcceptedOpinions());
            propertiesDict = new Text();
        } catch (RatingException e) {
            e.printStackTrace();
            setError(true);
            setErrorMessage(e.getMessage());
            Logger.getLogger(ListNotAcceptedBean.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * Akceptuje ocenę pracodawcy
     */
    public void acceptRating(){
        try {
            EmployerRating rating = ratingDataModel.getRowData();
            mopEndpoint.acceptOpinion(rating);
            addOperationMessage(false, "ratingaccepted");
        } catch (ApplicationException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(ListNotAcceptedBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }

    /**
     * Odrzuca ocenę pracodawcy
     */
    public void discardRating(){
        try {
            EmployerRating rating = ratingDataModel.getRowData();
            mopEndpoint.discardOpinion(rating);
            addOperationMessage(false, "ratingdiscarded");
        } catch (ApplicationException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(ListNotAcceptedBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }
    public DataModel<EmployerRating> getRatingDataModel() {
        return ratingDataModel;
    }

    public void setRatingDataModel(DataModel<EmployerRating> ratingDataModel) {
        this.ratingDataModel = ratingDataModel;
    }

    private void setError(boolean error) {
        this.error = error;
    }

    public boolean isError() {
        return error;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Dodanie komunikat o wyniku operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
     */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Dodanie komunikatu w przypadku niepowodzenia operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Wiadomość ze złapanego wyjątku
     */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
