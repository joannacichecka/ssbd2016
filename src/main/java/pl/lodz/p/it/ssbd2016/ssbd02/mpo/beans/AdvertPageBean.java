package pl.lodz.p.it.ssbd2016.ssbd02.mpo.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Advert;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.mok.endpoints.MOKEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.mpo.endpoints.MPOEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bean do wyświetlania ogłoszeń
 * @author Patryk Gutenplan
 */
@Named("advertPageBean")
@ViewScoped
public class AdvertPageBean implements Serializable {

    @EJB
    private MPOEndpointLocal mpoEndpoint;
    @EJB
    private MOKEndpointLocal mokEndpoint;

    private Advert currentAdvert;
    private Text propertiesDict;
    private String errorMessage;
    private boolean error = false;

    public Advert getCurrentAdvert() {
        return currentAdvert;
    }

    public AdvertPageBean(){
        propertiesDict = new Text();
    }

    /**
     * Inicjalizuje obiekty, pobiera z flash ogłoszenie do edycji
     */
    @PostConstruct
    public void init() {
        this.currentAdvert = (Advert) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("currentAdvertToShow");
    }

    /**
     * Przesłanie odpowiedzi na konkretne ogłoszenie
     *
     */
    public void advertResponse() {

        try {
            mpoEndpoint.sendResponse(currentAdvert);
            addOperationMessage(false, "responsecreated");
        } catch (ApplicationException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(AdvertPageBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }

    /**
     * Dodanie komunikat o wyniku operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
     */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Dodanie komunikatu w przypadku niepowodzenia operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Wiadomość ze złapanego wyjątku
     */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
