package pl.lodz.p.it.ssbd2016.ssbd02.exceptions;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;

import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

/**
 * Created by piotr on 16.05.16.
 */
public class CityException extends ApplicationException {
    public static final String KEY_OPTIMISTIC_LOCK_CITY_EDITION = "CITY.EXCEPTION.OPTIMISTIC_LOCK_CITY_EDITION";
    public static final String KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_CITY = "CITY.EXCEPTION.PERSISTENCE_EXCEPTION_NOT_UNIQUE_CITY";
    public static final String KEY_NO_CITIES_IN_DB = "CITY.EXCEPTION.NO_CITIES_IN_DB";
    public static final String KEY_NO_CITY_IN_DB = "CITY.EXCEPTION.NO_CITY_IN_DB";
    public static final String KEY_TRANSACTION_NOT_EXISTS = "CITY.EXCEPTION.TRANSACTION_NOT_EXISTS";
    public static final String KEY_UNKNOWN_ERROR = "CITY.EXCEPTION.UNKNOWN_ERROR";

    private City city;

    public CityException() {}
    public CityException(String message) {
        super(message);
    }
    public CityException(String message, Throwable cause) {
        super(message, cause);
    }

    public static CityException createCityEditionException(Throwable cause, City city) {
        return createException(cause, city, KEY_OPTIMISTIC_LOCK_CITY_EDITION);
    }

    public static CityException createNotUniqueException(Throwable cause, City city) {
        return createException(cause, city, KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_CITY);
    }

    public static CityException createNoCitiesInDbException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_NO_CITIES_IN_DB);

        CityException cityException = new CityException(message);
        return cityException;
    }

    public static CityException createNoCityInDbException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_NO_CITY_IN_DB);

        CityException cityException = new CityException(message);
        return cityException;
    }

    public static CityException createTransactionNotExistsException(Throwable cause, City city) {
    	return createException(cause, city, KEY_TRANSACTION_NOT_EXISTS);
    }
    
    public static CityException createUnknownErrorException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_UNKNOWN_ERROR);

        CityException cityException = new CityException(message);
        return cityException;
    }

    private static CityException createException(Throwable cause, City city, String msg) {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(msg);

        CityException cityException = new CityException(message, cause);
        cityException.setCity(city);
        return cityException;
    }
    
    private static CityException createException(String msg) {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(msg);

        CityException cityException = new CityException(message);
        return cityException;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
