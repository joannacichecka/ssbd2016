package pl.lodz.p.it.ssbd2016.ssbd02.mos.facades;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.CityException;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.*;

@Stateless(name = "CityFacadeMOS")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class CityFacade extends AbstractFacade<City> implements CityFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CityFacade() {
        super(City.class);
    }

    @Override
    @RolesAllowed("addCity")
    public void create(City city) throws CityException {
        try {
            em.persist(city);
            em.flush();
        } catch (TransactionRequiredException ex) {
            throw CityException.createTransactionNotExistsException(ex, city);
        } catch (PersistenceException pe) {
            if (pe.getMessage().contains("city_unique")) {
                throw CityException.createNotUniqueException(pe, city);
            } else {
                throw CityException.createUnknownErrorException();
            }
        }
    }

    @Override
    public City findByName(String name) throws CityException {
        try {
            Query query = em.createNamedQuery("City.findByName");
            query.setParameter("name", name);
            return (City) query.getSingleResult();
        } catch (NoResultException nre) {
            throw CityException.createNoCityInDbException();
        } catch (Exception e) {
            throw CityException.createUnknownErrorException();
        }
    }

    @Override
    public City findById(Object id) throws CityException {
        try {
            Query query = em.createNamedQuery("City.findById");
            query.setParameter("id", id);
            return (City) query.getSingleResult();
        } catch(NoResultException ex) {
            throw CityException.createNoCityInDbException();
        }

    }
    
    /**
     * Metoda wysyłająca miato do bazy
     * @param city Miasto po zmianach
     * @throws ApplicationException
     */
    @Override
    public void edit(City city) throws ApplicationException {
        try {
            em.merge(city);
            em.flush();
        } catch (OptimisticLockException o) {
            throw  CityException.createCityEditionException(o, city);
        } catch (TransactionRequiredException t) {
            throw  CityException.createTransactionNotExistsException(t, city);
        } catch (Exception e) {
            throw CityException.createUnknownErrorException();

        }
    }

}
