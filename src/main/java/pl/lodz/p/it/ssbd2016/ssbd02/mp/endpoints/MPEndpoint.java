package pl.lodz.p.it.ssbd2016.ssbd02.mp.endpoints;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.*;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.CityException;
import pl.lodz.p.it.ssbd2016.ssbd02.mok.beans.AccountSession;
import pl.lodz.p.it.ssbd2016.ssbd02.mp.facades.*;
import pl.lodz.p.it.ssbd2016.ssbd02.utils.CloneUtils;

import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.*;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by Joanna Cichecka on 2016-05-07.
 */
@Stateful
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class MPEndpoint implements MPEndpointLocal, SessionSynchronization {

    private static final Logger logger = Logger.getLogger(MPEndpoint.class.getName());

    @Resource
    private SessionContext sctx;
    @EJB
    private EmployerFacadeLocal employerFacade;
    @EJB
    private EmployeeFacadeLocal employeeFacade;
    @EJB
    private UniversityFacadeLocal universityFacade;
    @EJB
    private CityFacadeLocal cityFacade;
    @EJB
    private CategoryFacadeLocal categoryFacade;
    @EJB
    private AccountFacadeLocal accountFacade;
    private Employee currentEmployee;
    private Employer currentEmployer;
    private long transactionId;

    @Override
    @RolesAllowed("getEmployer")
    public Employer getEmployer(Employer employer) throws ApplicationException, IOException, ClassNotFoundException {
        currentEmployer = employerFacade.find(employer.getId());
        /* Referencing potentially null objects (after deserialization) to avoid ValidationException.java.
         * Source of nulls: FetchType = LAZY */
        currentEmployer.getEmployerLocationList().size();
        for (EmployerLocation el : currentEmployer.getEmployerLocationList()) {
            el.getCityId().getName();
        }
        currentEmployer.getCategoryList().size();
        return (Employer) CloneUtils.deepCloneThroughSerialization(currentEmployer);
    }

    @Override
    @RolesAllowed("getEmployee")
    public Employee getEmployee(Employee employee) throws ApplicationException, IOException, ClassNotFoundException {
        currentEmployee = employeeFacade.find(employee.getId());
        /* Referencing potentially null objects (after deserialization) to avoid ValidationException.java.
         * Source of nulls: FetchType = LAZY */
        currentEmployee.getCityId().toString();
        currentEmployee.getEmployeeLanguageList().size();
        currentEmployee.getEmployeeEducationList().size();
        currentEmployee.getEmployeeJobHistoryList().size();
        return (Employee) CloneUtils.deepCloneThroughSerialization(currentEmployee);
    }

    @Override
    @RolesAllowed("editEmployer")
    public void editEmployer(Employer employer) throws ApplicationException {
        currentEmployer.setDescription(employer.getDescription());
        currentEmployer.setName(employer.getName());

        for (EmployerLocation el : employer.getEmployerLocationList()) {
            if (el.getCityId().getId() == null) {
                el.setCityId(getOrCreateCity(el.getCityId().getName()));
            }
        }

        currentEmployer.setEmployerLocationList(employer.getEmployerLocationList());
        currentEmployer.setCategoryList(employer.getCategoryList());

        employerFacade.edit(currentEmployer);
        currentEmployer = null;
    }

    @Override
    @RolesAllowed("editEmployee")
    public void editEmployee(Employee employee, String cityName) throws ApplicationException {
        currentEmployee.setFirstName(employee.getFirstName());
        currentEmployee.setLastName(employee.getLastName());
        currentEmployee.setEmployeeEducationList(employee.getEmployeeEducationList());
        currentEmployee.setEmployeeJobHistoryList(employee.getEmployeeJobHistoryList());
        currentEmployee.setCityId(this.getOrCreateCity(cityName));
        currentEmployee.setEmployeeLanguageList(employee.getEmployeeLanguageList());
        currentEmployee.setPhoneNumber(employee.getPhoneNumber());

        employeeFacade.edit(currentEmployee);
        currentEmployee = null;
    }

    @Override
    @PermitAll
    public List<Employer> getAllEmployers() throws ApplicationException {
        return employerFacade.findAll();
    }

    @Override
    public Category findCategoryByName(String name) throws ApplicationException {
        return categoryFacade.findByName(name);
    }

    @Override
    public List<Category> getAllCategories() throws ApplicationException {
        return categoryFacade.findAll();
    }

    @Override
    public boolean isModerator() throws AccountException {
        return accountFacade.findByLogin(sctx.getCallerPrincipal().getName()).isModerator();
    }

    @Override
    @RolesAllowed("acceptProfile")
    public void acceptEmployer(Employer employer) throws ApplicationException {
        employer.setProfileConfirmed(true);
        employerFacade.edit(employer);
    }

    private City getOrCreateCity(String cityName) throws CityException {
        if (cityFacade.findByName(cityName) == null) {
            cityFacade.create(new City(cityName));
        }
        return cityFacade.findByName(cityName);
    }

    @Override
    public void afterBegin() throws EJBException, RemoteException {
        transactionId = System.currentTimeMillis();
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") has " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() + ".");
    }

    @Override
    public void beforeCompletion() throws EJBException, RemoteException {
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() +
                " before completion.");
    }

    @Override
    public void afterCompletion(boolean committed) throws EJBException, RemoteException {
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() +
                " was ended because of: " + (committed ? "COMMIT." : "ROLLBACK."));
    }

}
