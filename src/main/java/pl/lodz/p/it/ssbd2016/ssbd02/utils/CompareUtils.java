package pl.lodz.p.it.ssbd2016.ssbd02.utils;

import java.util.Collection;

/**
 * Created by ljaros on 4/20/16.
 */
public class CompareUtils {

    /**
     * Sprawdzenie czy kolekcja sourceCollection zawiera wszystkie elementy z newCollection
     * @param sourceCollection
     * @param newCollection
     * @return
     */
    public static boolean areCollectionsSame(Collection sourceCollection, Collection newCollection){
        if(sourceCollection.size() != newCollection.size()) return false;

        for (Object obj : newCollection) {
            if(!sourceCollection.contains(obj))
                return false;
        }

        return true;
    }
}
