package pl.lodz.p.it.ssbd2016.ssbd02.mop.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerRating;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.mop.endpoints.MOPEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named("ratingPageBean")
@ViewScoped
public class RatingPageBean implements Serializable {

    @EJB
    MOPEndpointLocal mopEndpoint;

    private EmployerRating employerRating;
    private Employer employer;
    private Text propertiesDict;
    private boolean error = false;
    private String errorMessage;
    private String comment;
    private Integer rating;

    public String getComment() { return comment; }

    public void setComment(String comment) { this.comment = comment; }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public void setEmployerRating(EmployerRating employerRating) {
        this.employerRating = employerRating;
    }

    public EmployerRating getEmployerRating() {
        return employerRating;
    }

    private void setError(boolean error) {
        this.error = error;
    }

    public boolean isError() {
        return error;
    }

    private void setErrorMessage(String errorMessage) { this.errorMessage = errorMessage; }

    public RatingPageBean() {
    }

    /**
     * Inicjaliacja pracownika oraz obiektu.
     */
    @PostConstruct
    public void init() {
        employerRating = new EmployerRating();
        propertiesDict = new Text();
        employer = (Employer) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("employerToRate");
    }

    /**
     * Wysylanie oceny.
     */
    public void sendRating() {
        try {
            employerRating.setAccepted(false);
            employerRating.setEmployerId(employer);
            mopEndpoint.addOpinion(employerRating);
            addOperationMessage(false, "ratingAdded");
        } catch (ApplicationException e) {
            setError(true);
            setErrorMessage(e.getMessage());
            Logger.getLogger(RatingPageBean.class.getName()).log(Level.SEVERE, null, e);
            addMessage(true, e.getMessage());
        }
    }

    /**
     * Dodanie komunikat o wyniku operacji
     *
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail  Nazwa property, której wartość wyświetlana jest użytkownikowi
     */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Dodanie komunikatu w przypadku niepowodzenia operacji
     *
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail  Wiadomość ze złapanego wyjątku
     */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}

