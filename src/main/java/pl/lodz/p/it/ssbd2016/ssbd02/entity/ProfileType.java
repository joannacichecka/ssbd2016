package pl.lodz.p.it.ssbd2016.ssbd02.entity;

public enum ProfileType {
    EMPLOYEE("EMPLOYEE"),
    EMPLOYER("EMPLOYER");

    private final String value;

    ProfileType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}