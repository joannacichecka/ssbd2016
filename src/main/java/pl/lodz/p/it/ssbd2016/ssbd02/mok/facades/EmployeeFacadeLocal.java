/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mok.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employee;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface EmployeeFacadeLocal {

    void create(Employee employee) throws ApplicationException;

    void edit(Employee employee) throws ApplicationException;

    void remove(Employee employee);

    Employee find(Object id) throws ApplicationException;

    List<Employee> findAll() throws ApplicationException;

    List<Employee> findRange(int[] range);

    int count();
    
}
