/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mteo.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Advert;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface AdvertFacadeLocal {

    /**
     * Zapisuje w bazie nowe ogłoszenie
     * @param advert Ogłoszenie do zapisu
     * @throws ApplicationException
     */
    void create(Advert advert) throws ApplicationException;

    /**
     * Edytuje istniejące już w bazie ogłoszenie
     * @param advert Zedytowane ogłoszenie
     * @throws ApplicationException
     */
    void edit(Advert advert) throws ApplicationException;

    /**
     * Usuwa z bazy danych podane ogłoszenie
     * @param advert Ogłoszenie do usunięcia
     */
    void remove(Advert advert);

    /**
     * Wyszukuje w bazie ogłoszenie o podanym ID
     * @param id ID ogłosznia
     * @return Ogłoszenie o podanym ID
     * @throws ApplicationException
     */
    Advert find(Object id) throws ApplicationException;

    /**
     * Zwraca listę wszystkich ogłoszeń
     * @return Lista wszystkich ogłoszeń
     * @throws ApplicationException
     */
    List<Advert> findAll() throws ApplicationException;

    /**
     * Zwraca listę ogłoszeń należących do danego Employera
      * @param employer Konto pracodawcy, którego ogłoszenia wyszukujemy
     * @return
     * @throws ApplicationException
     */
    List<Advert> findMyAdverts(Employer employer) throws ApplicationException;

    /**
     * Zwraca ogłoszenie o podanym ID
     * @param id ID ogłoszenia
     * @return
     * @throws ApplicationException
     */
    Advert findById(int id) throws ApplicationException;
    
}
