package pl.lodz.p.it.ssbd2016.ssbd02.exceptions;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;

import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

/**
 * Created by piotr on 16.05.16.
 */
public class EmployerException extends ApplicationException {
    public static final String KEY_OPTIMISTIC_LOCK_EMPLOYER_EDITION = "PROFILE.EXCEPTION" +
            ".OPTIMISTIC_LOCK_EMPLOYER_EDITION";
    public static final String KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_EMPLOYER = "PROFILE.EXCEPTION" +
            ".PERSISTENCE_EXCEPTION_NOT_UNIQUE_EMPLOYER";
    public static final String KEY_EMPLOYER_CONCURENT_MODIFICATION = "PROFILE.EXCEPTION" +
            ".EMPLOYER_CONCURENT_MODIFICATION";
    public static final String KEY_EMPLOYEER_CHANGED_AFTER_READING = "PROFILE.EXCEPTION.EMPLOYER_CHANGED_AFTER_READING";
    public static final String KEY_NO_EMPLOYER_IN_DB = "PROFILE.EXCEPTION.NO_EMPLOYER_IN_DB";
    public static final String KEY_UNKNOWN_ERROR = "PROFILE.EXCEPTION.UNKNOWN.ERROR";
    private Employer employer;

    public EmployerException() {
    }

    public EmployerException(String message) {
        super(message);
    }

    public EmployerException(String message, Throwable cause) {
        super(message, cause);
    }

    public static EmployerException createEmployerEditionException(Throwable cause, Employer employer) {
        return createException(cause, employer, KEY_OPTIMISTIC_LOCK_EMPLOYER_EDITION);
    }

    public static EmployerException createNotUniqueEmployer(Throwable cause, Employer employer) {
        return createException(cause, employer, KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_EMPLOYER);
    }

    public static EmployerException createConcurentModificationException(Throwable cause, Employer employer) {
        return createException(cause, employer, KEY_EMPLOYER_CONCURENT_MODIFICATION);
    }

    public static EmployerException createChangedAfterReadingException(Throwable cause, Employer employer) {
        return createException(cause, employer, KEY_EMPLOYEER_CHANGED_AFTER_READING);
    }

    public static EmployerException createNoEmployerInDbException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext
                .getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_NO_EMPLOYER_IN_DB);

        EmployerException employerException = new EmployerException(message);
        return employerException;
    }

    public static EmployerException createUnknownException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext
                .getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_UNKNOWN_ERROR);

        EmployerException employerException = new EmployerException(message);
        return employerException;
    }

    private static EmployerException createException(Throwable cause, Employer employer, String msg) {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext
                .getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(msg);

        EmployerException employerException = new EmployerException(message, cause);
        employerException.setEmployer(employer);
        return employerException;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public static ApplicationException createUnknownEmployerException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext
                .getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_UNKNOWN_ERROR);

        return new EmployeeException(message);
    }
}
