package pl.lodz.p.it.ssbd2016.ssbd02.mp.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employee;
import pl.lodz.p.it.ssbd2016.ssbd02.mp.endpoints.MPEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * Bean do wyświetlania profilu pracownika
 * Created by ljaros on 6/9/16.
 */
@Named("employeeProfileBean")
@RequestScoped
public class EmployeeProfileBean {
    @EJB
    private MPEndpointLocal mpEndpoint;
    private Employee employee;
    private String errorMessage;
    private boolean error = false;
    private Text propertiesDict;

    public EmployeeProfileBean() {
        this.propertiesDict = new Text();
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    /**
     * Inicjalizuje konto pracownika
     */
    @PostConstruct
    private void initModel(){
        employee = (Employee) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("employeeToShow");
    }

}
