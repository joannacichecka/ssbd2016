/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mop.facades;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employee;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.EmployeeException;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Fasada do zarządzania kontem pracownika
 * @author Patryk
 */
@Stateless(name = "EmployeeFacadeMOP")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class EmployeeFacade extends AbstractFacade<Employee> implements EmployeeFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployeeFacade() {
        super(Employee.class);
    }

    @Override
    @RolesAllowed("editEmployee")
    public void edit(Employee employee) throws ApplicationException {
        try {
            em.merge(employee);
            em.flush();
        } catch (OptimisticLockException e) {
            throw EmployeeException.createChangedAfterReadingException(e, employee);
        } catch (Exception e) {
            throw EmployeeException.createUnknownEmployeeException();
        }
    }
    @Override
    public Employee findByLogin(String login) throws ApplicationException {
        Query q = em.createNamedQuery("Employee.findByLogin");
        q.setParameter("login", login);
        return (Employee) q.getSingleResult();
    }
}
