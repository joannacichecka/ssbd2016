package pl.lodz.p.it.ssbd2016.ssbd02.mteo.endpoints;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Advert;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Category;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import java.util.Date;
import java.util.List;

/**
 * Created by Joanna Cichecka on 2016-05-07.
 */
public interface MTEOEndpointLocal {
    /**
     * Dodaje ogłoszenie wywołując odpowiedzialną za to funkcję z fasady.
     * Przed wywołaniem sprawdza poprawność ustawionej daty, ustawia miasto, kategorie oraz pracodawcę.
     * @param advert Ogłoszenie do dodania do bazy
     * @param cityName Nazwa miasta, do którego przypisano ogłoszenie
     * @param categories Lista kategorii, do których należy ogłoszenie
     * @throws ApplicationException
     */
    void addAdvert(Advert advert, String cityName, List<String> categories) throws ApplicationException;
    /**
     * Przekazuje do fasady ogłoszenie do edycji
     * Przed próbą edyji sprawdza poprawność ustawionej daty oraz czy ogłoszenie nie wygasło.
     * @param advert Ogłoszenie do edycji
     * @param cityName Nazwa miasta, do której przypisano ogłoszenie
     * @param oldDate Pierwotna data wygaśnięcia ogłoszenia - służy sprawdzaniu, czy ogłoszenie nie wygasło
     * @throws ApplicationException
     */
    void editAdvert(Advert advert, String cityName, Date oldDate) throws ApplicationException;
    /**
     * Usuwa ogłoszenie, wywołując odpowiednią metodę z fasady.
     * @param advert Ogłoszenie do usunięcia.
     * @throws ApplicationException
     */
    void deleteAdvert(Advert advert) throws ApplicationException;
    /**
     * Zwraca istniejące lub nowo utworzone miasto
     * Jeżeli miasto istnieje, to jest zwracane istniejące, w przeciwnym wypadku tworzone jest nowe i zwracane
     * @param cityName Nazwa miasta, które ma być wyszukane/dodane do bazy.
     * @return city Zwraca obiekt typu City.
     * @throws ApplicationException
     */
    City getOrCreateCity(String cityName) throws ApplicationException;
    /**
     * Zwraca listę wszystkich miast
     * @return Lista wszystkich miast w bazy danych
     * @throws ApplicationException
     */

    List<Category> getAllCategories() throws ApplicationException;
    /**
     * Zwraca aktualnie zalogowanego pracodaawcę
     * @return Zwraca obiekt typu Employee, przedstawiający aktualnie zalogowanego pracodawcę
     * @throws ApplicationException
     */
    Employer getEmployer() throws ApplicationException;
    /**
     * Zwraca listę obiektów kategorii, dla podanej listy Stringów przechowujacych nazwy kategorii.
     * @param categories lista nazw kategorii, które chcemy wyszukać.
     * @return konto pracodawcy
     * @throws ApplicationException
     */
    List<Category> getCategories(List<String> categories) throws ApplicationException;
    /**
     * Zwraca listę ogłoszeń należących do danego pracodawcy
     * @return lista wszystkich ogłoszeń pracodawcy
     * @throws ApplicationException
     */
    List<Advert> getMyAdverts() throws ApplicationException;
}
