package pl.lodz.p.it.ssbd2016.ssbd02.mok.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.EmailException;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Ziarno widoku rejestracji użytkownika
 * @author mgrabski
 */

@Named("registerPageBean")
@RequestScoped
public class RegisterPageBean {

	/**
	 * Domyślny konstruktor bezparametrowy - inicjalizuje zmienne account i 
	 * propertiesDict.
	 */
    public RegisterPageBean() {
        account = new Account();
        propertiesDict = new Text();
    }

    @Inject
    private AccountSession accountSession;

    private Account account;
    private Text propertiesDict;
    private String errorMessage;
    private boolean error = false;

    public String getErrorMessage() {
        return errorMessage;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    private void setError(boolean error) {
        this.error = error;
    }
    public Account getAccount() {
        return account;
    }

    /**
     * Metoda umożliwiająca rejestrację użytkownika.
     * @return
     * @throws AccountException
     */
    public String register() throws AccountException {
        try {
            accountSession.registerAccount(account);
            return "reg-success";
        } catch (AccountException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(CreatePageBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        } catch (EmailException e) {
            setError(true);
            setErrorMessage(e.getMessage());
            Logger.getLogger(CreatePageBean.class.getName()).log(Level.SEVERE, null, e);
            addMessage(true, e.getMessage());
        }

        return null;
    }

	/**
	 * Dodanie komunikatu w przypadku niepowodzenia operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Wiadomość ze złapanego wyjątku
	 */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
