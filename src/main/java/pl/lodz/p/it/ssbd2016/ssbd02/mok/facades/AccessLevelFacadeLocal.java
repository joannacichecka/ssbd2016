/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mok.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.AccessLevel;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface AccessLevelFacadeLocal {

    void create(AccessLevel accessLevel) throws ApplicationException;

    void edit(AccessLevel accessLevel) throws ApplicationException;

    void remove(AccessLevel accessLevel);

    AccessLevel find(Object id) throws ApplicationException;

    List<AccessLevel> findAll() throws AccountException;

    List<AccessLevel> findRange(int[] range);

    int count();
    
}
