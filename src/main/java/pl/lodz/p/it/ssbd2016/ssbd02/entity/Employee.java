/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * @author Patryk
 */
@Entity
@Table(name = "employee")
@NamedQueries({
        @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e"),
        @NamedQuery(name = "Employee.findById", query = "SELECT e FROM Employee e WHERE e.id = :id"),
        @NamedQuery(name = "Employee.findByFirstName", query = "SELECT e FROM Employee e WHERE e.firstName = " +
                ":firstName"),
        @NamedQuery(name = "Employee.findByLastName", query = "SELECT e FROM Employee e WHERE e.lastName = :lastName"),
        @NamedQuery(name = "Employee.findByPhoneNumber", query = "SELECT e FROM Employee e WHERE e.phoneNumber = " +
                ":phoneNumber"),
        @NamedQuery(name = "Employee.findByVersion", query = "SELECT e FROM Employee e WHERE e.version = :version"),
        @NamedQuery(name = "Employee.findByLogin", query = "SELECT e FROM Employee e WHERE e.account.login = :login")})
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "first_name")
    private String firstName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "last_name")
    private String lastName;
    @Basic(optional = false)
    @NotNull
    @Pattern(regexp = "[0-9]*")
    @Size(min = 1, max = 9)
    @Column(name = "phone_number")
    private String phoneNumber;
    @NotNull
    @Column(name = "version")
    @Version
    private int version;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "employeeId", orphanRemoval = true)
    private List<EmployeeJobHistory> employeeJobHistoryList;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "employeeId", orphanRemoval = true)
    private List<EmployeeLanguage> employeeLanguageList;
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Account account;
    @JoinColumn(name = "city_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private City cityId;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "employeeId", orphanRemoval = true)
    private List<EmployeeEducation> employeeEducationList;
    @OneToMany(mappedBy = "employeeId")
    private List<EmployerRating> employerRatingList;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "employeeId")
    private List<AdvertResponse> advertResponseList;

    public Employee() {
    }

    public Employee(Integer id) {
        this.id = id;
    }

    public Employee(Integer id, String firstName, String lastName, String phoneNumber, int version) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.version = version;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public List<EmployeeJobHistory> getEmployeeJobHistoryList() {
        return employeeJobHistoryList;
    }

    public void setEmployeeJobHistoryList(List<EmployeeJobHistory> employeeJobHistoryList) {
        this.employeeJobHistoryList = employeeJobHistoryList;
    }

    public List<EmployeeLanguage> getEmployeeLanguageList() {
        return employeeLanguageList;
    }

    public void setEmployeeLanguageList(List<EmployeeLanguage> employeeLanguageList) {
        this.employeeLanguageList = employeeLanguageList;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public City getCityId() {
        return cityId;
    }

    public void setCityId(City cityId) {
        this.cityId = cityId;
    }

    public List<EmployeeEducation> getEmployeeEducationList() {
        return employeeEducationList;
    }

    public void setEmployeeEducationList(List<EmployeeEducation> employeeEducationList) {
        this.employeeEducationList = employeeEducationList;
    }

    public List<EmployerRating> getEmployerRatingList() {
        return employerRatingList;
    }

    public void setEmployerRatingList(List<EmployerRating> employerRatingList) {
        this.employerRatingList = employerRatingList;
    }

    public List<AdvertResponse> getAdvertResponseList() {
        return advertResponseList;
    }

    public void setAdvertResponseList(List<AdvertResponse> advertResponseList) {
        this.advertResponseList = advertResponseList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.ssbd2016.ssbd02.entity.Employee[ id=" + id + " ]";
    }

}
