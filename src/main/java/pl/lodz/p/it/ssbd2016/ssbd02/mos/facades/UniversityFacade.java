/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mos.facades;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.University;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.UniversityException;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.*;


/**
 * @author Patryk
 */
@Stateless(name = "UniversityFacadeMOS")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class UniversityFacade extends AbstractFacade<University> implements UniversityFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UniversityFacade() {
        super(University.class);
    }

    @Override
    public University findByName(String name) throws UniversityException {
        try {
            Query query = em.createNamedQuery("University.findByName");
            query.setParameter("name", name);
            return (University) query.getSingleResult();
        } catch (NoResultException nre) {
            throw UniversityException.createNoUniversityInDbException();
        }
    }
    public University findById(Object id) throws UniversityException {
        try {
            Query query = em.createNamedQuery("University.findById");
            query.setParameter("id", id);
            return (University) query.getSingleResult();
        } catch(NoResultException ex) {
            throw UniversityException.createNoUniversityInDbException();
        }
    }

    @Override
    public void edit(University university) throws ApplicationException {
        try {
            em.merge(university);
            em.flush();
        } catch(OptimisticLockException ole) {
            throw UniversityException.createUniversityEditionException(ole, university);
        } catch(TransactionRequiredException tre) {
            throw UniversityException.createTransactionNotExistException(tre, university);
        } catch(Exception e) {
            throw UniversityException.createUnknownErrorException();
        }
    }
}
