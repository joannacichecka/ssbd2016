package pl.lodz.p.it.ssbd2016.ssbd02.mpo.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Advert;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.AdvertResponse;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AdvertException;
import pl.lodz.p.it.ssbd2016.ssbd02.mpo.endpoints.MPOEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by piotr on 06.06.16.
 * Bean wyświetlający listę ogłoszeń
 */
@Named("listAdvertBean")
@RequestScoped
public class ListAdvertBean {

    @EJB
    private MPOEndpointLocal mpoEndpoint;

    private DataModel<Advert> advertDataModel;
    private Text propertiesDict;
    private String errorMessage;
    private boolean error = false;

    public String getErrorMessage() {
        return errorMessage;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    private void setError(boolean error) {
        this.error = error;
    }

    @PostConstruct
    public void init() {
        try {
            this.advertDataModel = new ListDataModel<>(mpoEndpoint.getAdverts());
        } catch (AdvertException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(ListAdvertBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public DataModel<Advert> getAdvertDataModel() {
        return advertDataModel;
    }

    public void setAdvertDataModel(DataModel<Advert> advertDataModel) {
        this.advertDataModel = advertDataModel;
    }

    /**
     * Metoda przekazując wybrene ogłoszenie do szegółowego wyświetlania
     *
     * @param id - Id wybranego ogłoszenia
     */
    public void setCurrentAdvert(Integer id) {
        Advert currentAdvertToShow = null;
        try {
            currentAdvertToShow = mpoEndpoint.getAdvert(id);
        } catch (AdvertException | IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        currentAdvertToShow = (Advert) FacesContext.getCurrentInstance().getExternalContext().getFlash().put("currentAdvertToShow", currentAdvertToShow);
    }


}
