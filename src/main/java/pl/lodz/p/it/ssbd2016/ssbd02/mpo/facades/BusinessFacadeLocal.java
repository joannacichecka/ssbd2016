/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mpo.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Business;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface BusinessFacadeLocal {

    void create(Business business) throws ApplicationException;

    void edit(Business business) throws ApplicationException;

    void remove(Business business);

    Business find(Object id) throws ApplicationException;

    List<Business> findAll() throws ApplicationException;

    List<Business> findRange(int[] range);

    int count();
    
}
