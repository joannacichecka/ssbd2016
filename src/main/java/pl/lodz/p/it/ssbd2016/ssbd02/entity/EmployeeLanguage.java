/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 *
 * @author Patryk
 */
@Entity
@Table(name = "employee_language")
@NamedQueries({
    @NamedQuery(name = "EmployeeLanguage.findAll", query = "SELECT e FROM EmployeeLanguage e"),
    @NamedQuery(name = "EmployeeLanguage.findByLanguage", query = "SELECT e FROM EmployeeLanguage e WHERE e.language = :language"),
    @NamedQuery(name = "EmployeeLanguage.findByLevel", query = "SELECT e FROM EmployeeLanguage e WHERE e.level = :level"),
    @NamedQuery(name = "EmployeeLanguage.findById", query = "SELECT e FROM EmployeeLanguage e WHERE e.id = :id")})
public class EmployeeLanguage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "language")
    private String language;
    @Size(max = 2)
    @Column(name = "level")
    private String level;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "employee_id", referencedColumnName = "id", updatable = false)
    @ManyToOne(optional = false)
    private Employee employeeId;

    public EmployeeLanguage() {
    }

    public EmployeeLanguage(Integer id) {
        this.id = id;
    }

    public EmployeeLanguage(Employee employeeId) {
        this.employeeId = employeeId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Employee getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employee employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeeLanguage)) {
            return false;
        }
        EmployeeLanguage other = (EmployeeLanguage) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployeeLanguage[ id=" + id + " ]";
    }
    
}
