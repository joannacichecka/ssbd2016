package pl.lodz.p.it.ssbd2016.ssbd02.exceptions;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employee;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;

import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

/**
 * Created by piotr on 16.05.16.
 */
public class EmployeeException extends ApplicationException {
    public static final String KEY_OPTIMISTIC_LOCK_EMPLOYEE_EDITION = "PROFILE.EXCEPTION.OPTIMISTIC_LOCK_EMPLOYEE_EDITION";
    public static final String KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_EMPLOYEE = "PROFILE.EXCEPTION.PERSISTENCE_EXCEPTION_NOT_UNIQUE_EMPLOYERE";
    public static final String KEY_EMPLOYEE_CONCURENT_MODIFICATION = "PROFILE.EXCEPTION.EMPLOYEE_CONCURENT_MODIFICATION";
    public static final String KEY_EMPLOYEE_CHANGED_AFTER_READING = "PROFILE.EXCEPTION.EMPLOYEE_CHANGED_AFTER_READING";
    public static final String KEY_NO_EMPLOYEE_IN_DB = "PROFILE.EXCEPTION.NO_EMPLOYEE_IN_DB";
    public static final String KEY_UNKNOWN_ERROR = "ACCOUNT.EXCEPTION.UNKNOWN_ERROR";
    private Employee employee;

    public EmployeeException() {}
    public EmployeeException(String message) {
        super(message);
    }
    public EmployeeException(String message, Throwable cause) {
        super(message, cause);
    }

    public static EmployeeException createEmployeeEditionException(Throwable cause, Employee employee) {
        return createException(cause, employee, KEY_OPTIMISTIC_LOCK_EMPLOYEE_EDITION);
    }

    public static EmployeeException createConcurentModificationException(Throwable cause, Employee employee) {
        return createException(cause, employee, KEY_EMPLOYEE_CONCURENT_MODIFICATION);
    }

    public static EmployeeException createChangedAfterReadingException(Throwable cause, Employee employee) {
        return createException(cause, employee, KEY_EMPLOYEE_CHANGED_AFTER_READING);
    }

    public static EmployeeException createNotUniqueEmployeeException(Throwable cause, Employee employee) {
        return createException(cause, employee, KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_EMPLOYEE);
    }

    public static EmployeeException createNoEmployeeInDbException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_NO_EMPLOYEE_IN_DB);

        EmployeeException employeeException = new EmployeeException(message);
        return employeeException;
    }

    public static EmployeeException createUnknownEmployeeException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_UNKNOWN_ERROR);

        EmployeeException employeeException = new EmployeeException(message);
        return employeeException;
    }

    private static EmployeeException createException(Throwable cause, Employee employee, String msg) {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(msg);

        EmployeeException employeeException = new EmployeeException(message, cause);
        employeeException.setEmployee(employee);
        return employeeException;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

}
