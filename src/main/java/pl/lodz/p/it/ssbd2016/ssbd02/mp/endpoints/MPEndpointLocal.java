package pl.lodz.p.it.ssbd2016.ssbd02.mp.endpoints;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Category;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employee;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

import java.io.IOException;
import java.util.List;

public interface MPEndpointLocal {

    /**
     * Pobiera z bazy danych aktualne dane obiektu Employer.
     * @param employer
     * @return obiekt Employer
     * @throws ApplicationException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    Employer getEmployer(Employer employer) throws ApplicationException, IOException, ClassNotFoundException;

    /**
     * Pobiera z bazy danych aktualne dane obiektu Employer.
     * @param employee
     * @return obiekt Employee
     * @throws ApplicationException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    Employee getEmployee(Employee employee) throws ApplicationException, IOException, ClassNotFoundException;

    /**
     * Zapisuje podany obiekt klasy Employer do bazy danych.
     * @param oldEmployer
     * @throws ApplicationException
     */
    void editEmployer(Employer oldEmployer) throws ApplicationException;

    /**
     * Zapisuje podany obiekt klasy Employee do bazy danych.
     * @param employee
     * @param cityName
     * @throws ApplicationException
     */
    void editEmployee(Employee employee, String cityName) throws ApplicationException;

    /**
     * Zwraca listę wszystkich pracodawców z bazy danych.
     * @return lista pracodawców
     * @throws ApplicationException
     */
    List<Employer> getAllEmployers() throws ApplicationException;

    /**
     * Zwraca listę wszystkich pracowników z bazy danych.
     * @param name
     * @return lista pracowników
     * @throws ApplicationException
     */
    Category findCategoryByName(String name) throws ApplicationException;

    /**
     * Zwraca listę wszystkich kategorii z bazy danych.
     * @return lista kategorii
     * @throws ApplicationException
     */
    List<Category> getAllCategories() throws ApplicationException;

    /**
     * Sprawdza czy konto posiada uprawnienia moderatora
     * @return wartość logiczna odpowiadająca, czy konto ma uprawnienie moderatora
     * @throws AccountException
     */
    boolean isModerator() throws AccountException;

    /**
     * Akceptuje konto pracodawcy
     * @param employer konto pracodawcy
     * @throws ApplicationException
     */
    void acceptEmployer(Employer employer) throws ApplicationException;
}
