/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mok.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;

/**
 *
 * @author Patryk
 */
@Local
public interface AccountFacadeLocal {

    void create(Account account) throws AccountException;
    void edit(Account account) throws AccountException;
    void remove(Account account);
    Account find(Object id) throws AccountException;
    List<Account> findAll() throws AccountException;
    List<Account> findRange(int[] range);
    int count();
    Account findByLogin(String login) throws AccountException;    
}
