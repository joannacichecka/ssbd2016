package pl.lodz.p.it.ssbd2016.ssbd02.mos.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.University;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.mos.endpoints.MOSEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named("listUniBean")
@RequestScoped
public class ListUniBean {
    private static final String OUTCOME_PATH = "edit-university";

    @EJB
    private MOSEndpointLocal mosEndpoint;
    private DataModel<University> uniDataModel;
    private Text propertiesDict;
    private boolean error = false;
    private String errorMessage;
    private String cityName;
    private University currentUniversity;
    private List<City> cityList;

    @PostConstruct
    public void initModel() {
        try {
            uniDataModel = new ListDataModel<>(mosEndpoint.getUniversities());
            this.cityList = mosEndpoint.getCities();
            propertiesDict = new Text();
            currentUniversity = null;
        } catch (ApplicationException e) {
            e.printStackTrace();
            setError(true);
            setErrorMessage(e.getMessage());
            addMessage(true, e.getMessage());
            Logger.getLogger(ListUniBean.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public String initEdit() {
        currentUniversity = uniDataModel.getRowData();
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("universityToEdit", currentUniversity);
        return OUTCOME_PATH;
    }

    public DataModel<University> getUniDataModel() {
        return uniDataModel;
    }

    public void setUniDataModel(DataModel<University> uniDataModel) {
        this.uniDataModel = uniDataModel;
    }

    private void setError(boolean error) {
        this.error = error;
    }

    public boolean isError() {
        return error;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<City> getCityList() {
        return cityList;
    }

    public void setCityList(List<City> cityList) {
        this.cityList = cityList;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * Dodanie komunikatu w przypadku niepowodzenia operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Wiadomość ze złapanego wyjątku
     */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
