package pl.lodz.p.it.ssbd2016.ssbd02.mteo.endpoints;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.SessionContext;
import javax.ejb.SessionSynchronization;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.*;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AdvertException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import pl.lodz.p.it.ssbd2016.ssbd02.mteo.facades.*;

/**
 * Created by Joanna Cichecka on 2016-05-07.
 */

/**
 * Realizuje logikę bizensową oraz wywołuje metody z fasad, celem pobrania, edycji i zapisu obiektów encji do bazy
 * danych.
 */
@Stateful
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class MTEOEndpoint implements MTEOEndpointLocal, SessionSynchronization {
    private static final Logger logger = Logger.getLogger(MTEOEndpoint.class.getName());

    @Resource
    private SessionContext sctx;
    @EJB
    private AdvertFacadeLocal advertFacade;
    @EJB
    private CityFacadeLocal cityFacade;
    @EJB
    private CategoryFacadeLocal categoryFacade;
    @EJB
    private EmployerFacadeLocal employerFacade;
    private Advert currentAdvert;
    private long transactionId;

    @Override
    @RolesAllowed("addAdvert")
    public void addAdvert(Advert advert, String cityName, List<String> categories) throws ApplicationException {
        advert.setCreationTime(new Date());
        if(advert.getCreationTime().after(advert.getExpiryDate()))
            throw AdvertException.createWrongDateException();
        advert.setCityId(getOrCreateCity(cityName));
        advert.setEmployerId(getEmployer());
        advert.setAdvertResponseList(new ArrayList<AdvertResponse>());
        advert.setCategoryList(getCategories(categories));
        advertFacade.create(advert);
    }

    @Override
    @RolesAllowed("editAdvert")
    public void editAdvert(Advert advert, String cityName, Date oldDate) throws ApplicationException {
        if(oldDate.before(new Date()))
            throw AdvertException.createAdvertTerminatedException(new Throwable(), advert);
        if(advert.getExpiryDate().before(new Date()))
            throw AdvertException.createWrongDateException();

        advert.setCityId(getOrCreateCity(cityName));
        advertFacade.edit(advert);
    }

    @Override
    @RolesAllowed("addCity")
    public City getOrCreateCity(String cityName) throws ApplicationException {
        if (cityFacade.findByName(cityName) == null) {
            cityFacade.create(new City(cityName));
        }
        return cityFacade.findByName(cityName);
    }

    @Override
    @RolesAllowed("deleteAdvert")
    public void deleteAdvert(Advert advert) throws ApplicationException {
    	currentAdvert = advertFacade.find(advert.getId());
    	advertFacade.remove(currentAdvert);

        currentAdvert = null;
    }

    @Override
    @RolesAllowed("getAllCategories")
    public List<Category> getAllCategories() throws ApplicationException {
        return categoryFacade.findAll();
    }

    @Override
    @RolesAllowed("findByAccount")
    public Employer getEmployer() throws ApplicationException {
        return employerFacade.findByAccout(sctx.getCallerPrincipal().getName());
    }

    @Override
    @RolesAllowed("getCategories")
    public List<Category> getCategories(List<String> categories) throws ApplicationException {
        return categoryFacade.findList(categories);
    }

    @Override
    @RolesAllowed("getMyAdverts")
    public List<Advert> getMyAdverts() throws ApplicationException {
        Employer employer = getEmployer();
        return advertFacade.findMyAdverts(employer);
    }

    /**
     * Loguje informacje o rozpoczęciu transakcji
     * @throws EJBException
     * @throws RemoteException
     */
    @Override
    public void afterBegin() throws EJBException, RemoteException {
        transactionId = System.currentTimeMillis();
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") has " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() + ".");
    }

    /**
     * Loguje informacje przed zakończeniem transakcji
     * @throws EJBException
     * @throws RemoteException
     */
    @Override
    public void beforeCompletion() throws EJBException, RemoteException {
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() +
                " before completion.");
    }

    /**
     * Loguje informacje o zakończonej transakcji
     * @param committed Informacja o zatwierdzeniu (true) lub przerwaniu transakcji (false)
     * @throws EJBException
     * @throws RemoteException
     */
    @Override
    public void afterCompletion(boolean committed) throws EJBException, RemoteException {
        logger.log(Level.SEVERE, "Transaction (ID:" + transactionId + ") " +
                "started by user with ID: " + sctx.getCallerPrincipal().getName() +
                " was ended because of: " + (committed ? "COMMIT." : "ROLLBACK."));
    }
}
