package pl.lodz.p.it.ssbd2016.ssbd02.exceptions;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Category;

import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

/**
 * Created by piotr on 01.06.16.
 */
public class CategoryException extends ApplicationException {
    public static final String KEY_NO_CATEGORIES_IN_DB = "CATEGORY.EXCEPTION.NO_CATEGORIES_IN_DB";
    public static final String KEY_UNKNOWN_ERROR= "CATEGORY.EXCEPTION.UNKNOWN.EXCEPTION";
    private Category category;

    public CategoryException() {};
    public CategoryException(String message) {
        super(message);
    }
    public CategoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public static CategoryException createNoCategoriesInDbException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_NO_CATEGORIES_IN_DB);

        CategoryException categoryException = new CategoryException(message);
        return categoryException;
    }

    public static CategoryException createUnknownException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text",
                FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_UNKNOWN_ERROR);

        CategoryException categoryException = new CategoryException(message);
        return categoryException;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
