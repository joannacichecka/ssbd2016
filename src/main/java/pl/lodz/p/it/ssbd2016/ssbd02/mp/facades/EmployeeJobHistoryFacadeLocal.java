/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mp.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployeeJobHistory;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface EmployeeJobHistoryFacadeLocal {

    void create(EmployeeJobHistory employeeJobHistory) throws ApplicationException;

    void edit(EmployeeJobHistory employeeJobHistory) throws ApplicationException;

    void remove(EmployeeJobHistory employeeJobHistory);

    EmployeeJobHistory find(Object id) throws ApplicationException;

    List<EmployeeJobHistory> findAll() throws ApplicationException;

    List<EmployeeJobHistory> findRange(int[] range);

    int count();
    
}
