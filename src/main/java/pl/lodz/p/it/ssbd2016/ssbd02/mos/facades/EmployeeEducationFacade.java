/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mos.facades;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployeeEducation;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;

/**
 *
 * @author Patryk
 */
@Stateless(name = "EmployeeEducationFacadeMOS")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class EmployeeEducationFacade extends AbstractFacade<EmployeeEducation> implements EmployeeEducationFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployeeEducationFacade() {
        super(EmployeeEducation.class);
    }
    
}
