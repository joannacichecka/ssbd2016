package pl.lodz.p.it.ssbd2016.ssbd02.mpo.beans;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import java.util.List;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AdvertResponse;
import pl.lodz.p.it.ssbd2016.ssbd02.mpo.endpoints.MPOEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

/**
 * @author Patryk Gutenplan
 * Bean wyświetlający listę odpowiedzi
 */
@Named("responseListBean")
@RequestScoped
public class ResponseListBean {
	
	@EJB
	private MPOEndpointLocal mpoEndpoint;
	private DataModel<AdvertResponse> advertResponseDataModel;
	private String errorMessage;
    private boolean error = false;
    private Text propertiesDict;
    
	public ResponseListBean() {
		this.propertiesDict = new Text();
	}
	public DataModel<AdvertResponse> getAdvertResponseDataModel() {
		return advertResponseDataModel;
	}
	public void setAdvertResponseDataModel(DataModel<AdvertResponse> advertResponseDataModel) {
		this.advertResponseDataModel = advertResponseDataModel;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	public Text getPropertiesDict() {
		return propertiesDict;
	}
	public void setPropertiesDict(Text propertiesDict) {
		this.propertiesDict = propertiesDict;
	}

    /**
	 * Inicjalizuje obiekt, pobierając odpowiedź na ogłoszenie
	 */
	@PostConstruct
	public void initModel(){
		advertResponseDataModel = new ListDataModel<AdvertResponse>((List<AdvertResponse>) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("advertResponseList"));
	}
    
}
