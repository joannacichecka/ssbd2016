package pl.lodz.p.it.ssbd2016.ssbd02.mok.beans;

import org.primefaces.event.RowEditEvent;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.EmailException;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Ziarno widoku listy wszystkich użytkowników.
 * @author mgrabski
 *
 */

@Named("ListPageBeanMOK")
@RequestScoped
public class ListPageBean {
    
	public ListPageBean() {
    }

    @Inject
    private AccountSession accountSession;

    private DataModel<Account> accountsDataModel;
    private String errorMessage;
    private Text propertiesDict;
    private boolean error = false;

    /**
     * Inicjalizacja modelu - metoda jest wykonywana automatycznie po załadowaniu strony
     * i wczytuje listę wszystkich kont użytkowników.
     */
    @PostConstruct
    public void initModel() {
        try {
            accountSession.setAccountToEdit(null);
            propertiesDict = new Text();
            accountsDataModel = new ListDataModel<Account>(accountSession.getAllAccounts());
        } catch (AccountException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(ListPageBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    private void setError(boolean error) {
        this.error = error;
    }

    public DataModel<Account> getAccountsDataModel() {
        return accountsDataModel;
    }

    /**
     * Metoda umożliwiająca aktywację konta wybranego z listy.
     */
    public void activateAccount() {
        try {
            Account activatedAccount = accountsDataModel.getRowData();
            accountSession.activateAccount(activatedAccount);
            addOperationMessage(false, "accountactivated");
        } catch (AccountException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(ListPageBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        } catch (EmailException e) {
            setError(true);
            setErrorMessage(e.getMessage());
            Logger.getLogger(ListPageBean.class.getName()).log(Level.SEVERE, null, e);
            addMessage(true, e.getMessage());
        }

    }

    /**
     * Metoda umożliwia deaktywację konta wybranego z listy.
     */
    public void deactivateAccount() {
        try {
            Account deactivatedAccount = accountsDataModel.getRowData();
            accountSession.deactivateAccount(deactivatedAccount);
            addOperationMessage(false, "accountdeactivated");
        } catch (AccountException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(ListPageBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        } catch (EmailException e) {
            setError(true);
            setErrorMessage(e.getMessage());
            Logger.getLogger(ListPageBean.class.getName()).log(Level.SEVERE, null, e);
            addMessage(true, e.getMessage());
        }
    }

    /**
     * Metoda umożliwia edycję konta wybranego na liście użytkowników.
     */
    private void editAccount() {
        try {
            Account editedAccount = accountsDataModel.getRowData();
            accountSession.setAccountToEdit(editedAccount);
            accountSession.editAccount(accountSession.getAccountToEdit(editedAccount), editedAccount);
            addOperationMessage(false, "accountedited");
        } catch (AccountException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(ListPageBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }

    /**
     * Metoda umożliwiająca edycję poziomów dostępu dla wybranego konta z 
     * poziomu listy użytkowników.
     */
    public void changeAccessLevels() {
        try {
            Account changedAccount = accountsDataModel.getRowData();
            accountSession.changeAccessLevels(changedAccount);
            addOperationMessage(false, "accesslevelschanged");
        } catch (AccountException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(ListPageBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }

    /**
     * Metoda przygotowuje widok zmiany hasła użytkownika wybranego z listy.
     * @return Zwraca przekierowanie do formularza xhtml.
     */
    public String prepareChangePasswordView() {
            Account changedAccount = accountsDataModel.getRowData();
            accountSession.setAccountToEdit(changedAccount);
            return "admin-new-password";
    }

    /**
     * Umożliwia edycję danych użytkownika bezpośrednio przy pomocy załadowanej listy
     * użytkowników.
     */
    public void onRowEdit(RowEditEvent event) {
        try {
            editAccount();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	/**
	 * Dodanie komunikatu o wyniku operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
	 */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

	/**
	 * Dodanie komunikatu w przypadku niepowodzenia operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Wiadomość ze złapanego wyjątku
	 */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
