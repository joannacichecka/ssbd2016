/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mpo.facades;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Advert;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AdvertException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * Zarządza ogłoszeniami
 * @author Patryk
 */
@Stateless(name = "AdvertFacadeMPO")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class AdvertFacade extends AbstractFacade<Advert> implements AdvertFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mpoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdvertFacade() {
        super(Advert.class);
    }

    @Override
    @RolesAllowed("searchForAdvert")
    public List<Advert> searchForAdvert(String phrase, String cityName) throws AdvertException {
        try {
            Query query = em.createNamedQuery("Advert.findByQuery");
            query.setParameter("phrase", phrase);
            query.setParameter("city", cityName);
            query.setParameter("expiryDate", new Date());
            return (List<Advert>)query.getResultList();
        } catch(NoResultException e) {
            throw AdvertException.createNoEntitiesException();
        }

    }

    @Override
    @RolesAllowed("searchForAdvert")
    public List<Advert> searchForAdvert(String cityName) throws AdvertException {
        try {
            Query query = em.createNamedQuery("Advert.findByCityAndDate");
            query.setParameter("city", cityName);
            query.setParameter("expiryDate", new Date());
            return (List<Advert>)query.getResultList();
        } catch(NoResultException e) {
            throw AdvertException.createNoEntitiesException();
        }
    }

    @Override
    @PermitAll
    public List<Advert> findAll() throws AdvertException {
        try {
            return em.createNamedQuery("Advert.findAll").getResultList();
        } catch (NoResultException ex) {
            throw AdvertException.createNoEntitiesException();
        } catch (Exception e) {
            throw AdvertException.createUnknownErrorException();
        }
    }

    @Override
    @PermitAll
    public Advert findById(Integer id) throws AdvertException {
        try {
            return (Advert) em.createNamedQuery("Advert.findById").setParameter("id",id).getSingleResult();
        } catch (NoResultException ex) {
            throw AdvertException.createNoEntitiesException();
        } catch (Exception e) {
            throw AdvertException.createUnknownErrorException();
        }
    }

    @Override
    @PermitAll
    public List<Advert> findByExpiryDate(Date date) throws AdvertException {
        try {
            return (List<Advert>) em.createNamedQuery("Advert.findByExpiryDate").setParameter("expiryDate", date).getResultList();
        } catch (NoResultException ex) {
            throw AdvertException.createNoEntitiesException();
        } catch (Exception e) {
            throw AdvertException.createUnknownErrorException();
        }
    }

    @Override
    @RolesAllowed("editAdvert")
    public void edit(Advert advert) throws ApplicationException {
        try {
            em.merge(advert);
            em.flush();
        } catch (OptimisticLockException ex) {
            throw AdvertException.createAdvertEditionException(ex, advert);
        } catch (TransactionRequiredException ex) {
            throw AdvertException.createTransactionNotExistException(ex, advert);
        } catch (Exception ex) {
            throw AdvertException.createUnknownErrorException();
        }
    }

    public List<Advert> findByQuery(String phrase, String cityName) {
        Query query = em.createNamedQuery("Advert.findByQuery");
        query.setParameter("phrase", phrase);
        query.setParameter("city", cityName);
        List<Advert> adverts= query.getResultList();
        return adverts;
    }
}
