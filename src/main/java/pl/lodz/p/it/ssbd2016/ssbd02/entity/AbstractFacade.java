/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.entity;

import java.util.List;
import javax.persistence.EntityManager;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    /**
     * Konstruktor klasy.
     * @param entityClass
     */
    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    /**
     * Przeprowadza dodawanie do bazy podanego obiektu.
     * @param entity dodawana encja
     * @throws ApplicationException
     */
    public void create(T entity) throws ApplicationException {
        getEntityManager().persist(entity);
    }

    /**
     * Przeprowadza edycję podanego obiektu w bazie.
     * @param entity edytowana encja
     * @throws ApplicationException
     */
    public void edit(T entity) throws ApplicationException {
        getEntityManager().merge(entity);
    }

    /**
     * Przeprowadza usuwanie z bazy podanego obiektu.
     * @param entity usuwana encja
     */
    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    /**
     * Przeprowadza pobranie z bazy obiektu o podanym id.
     * @param id szukane id
     * @return szukany obiekt z bazy
     * @throws ApplicationException
     */
    public T find(Object id) throws ApplicationException {
        return getEntityManager().find(entityClass, id);
    }

    /**
     * Przeprowadza pobranie wszystkich obiektów danego typu w bazie.
     * @return lista obiektów danego typu z bazy
     * @throws ApplicationException
     */
    public List<T> findAll() throws ApplicationException {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    /**
     * Przeprowadza szukanie obiektów danego typu z bazy w podanym przedziale.
     * @param range podany przedział
     * @return lista obiektów danego typu z bazy
     */
    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    /**
     * Przeprowadza liczenie obiektów w bazie.
     * @return liczba obiektów
     */
    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
}
