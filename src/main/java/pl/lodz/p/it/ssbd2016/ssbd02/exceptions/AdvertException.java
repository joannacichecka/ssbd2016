package pl.lodz.p.it.ssbd2016.ssbd02.exceptions;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Advert;

import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

/**
 * Created by piotr on 16.05.16.
 */
public class AdvertException extends ApplicationException {
    public static final String KEY_OPTIMISTIC_LOCK_ADVERT_EDITION = "ADVERT.EXCEPTION.OPTIMISTIC_LOCK_ADVERT_EDITION";
    public static final String KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_ADVERT = "ADVERT.EXCEPTION.PERSISTENCE_EXCEPTION_NOT_UNIQUE_ADVERT";
    public static final String KEY_ADVERT_CONCURENT_MODIFICATION = "ADVERT.EXCEPTION.ADVERT_CONCURRENT_MODIFICATION";
    public static final String KEY_ADVERT_CHANGED_AFTER_READING = "ADVERT.EXCEPTION.ADVERT_CHANGED_AFTER_READING";
    public static final String KEY_ADVERT_TERMINATED = "ADVERT.EXCEPTION.ADVERT_TERMINATED";
    public static final String KEY_TRANSTACTION_NOT_EXIST = "ADVERT.EXCEPTION.TRANSACTION_NOT_EXIST";
    public static final String KEY_RESPONSES_LIMIT_EXCEEDED = "ADVERT.EXCEPTION.RESPONSES_LIMIT_EXCEEDED";
    public static final String KEY_NO_ENTITY_IN_DB = "ADVERT.EXCEPTION.NO_ENTITY_IN_DB";
    public static final String KEY_NO_ENTITIES_IN_DB = "ADVERT.EXCEPTION.NO_ENTITIES_IN_DB";
    public static final String KEY_NO_RESPONSES_IN_DB = "ADVERT.EXCEPTION.NO_RESPONSES_IN_DB";
    public static final String KEY_UNKNOWN_ERROR = "ADVERT.EXCEPTION.UNKNOWN_ERROR";
    public static final String KEY_WRONG_DATE = "ADVERT.EXCEPTION.WRONG_DATE";
    private Advert advert;

    public AdvertException() {}
    public AdvertException(String message) {
        super(message);
    }
    public AdvertException(String message, Throwable cause) {
        super(message, cause);
    }

    public static AdvertException createAdvertEditionException(Throwable cause, Advert advert) {
        return createAdvertException(cause, advert, KEY_OPTIMISTIC_LOCK_ADVERT_EDITION);
    }

    public static AdvertException createNotUniqueAdvertException(Throwable cause, Advert advert) {
        return createAdvertException(cause, advert, KEY_PERSISTENCE_EXCEPTION_NOT_UNIQUE_ADVERT);
    }

    public static AdvertException createConcurentModificationException(Throwable cause, Advert advert) {
        return createAdvertException(cause, advert, KEY_ADVERT_CONCURENT_MODIFICATION);
    }

    public static AdvertException createChangedAfterReadingException(Throwable cause, Advert advert) {
        return createAdvertException(cause, advert, KEY_ADVERT_CHANGED_AFTER_READING);
    }

    public static AdvertException createNoResponsesException(Throwable cause, Advert advert) {
        return createAdvertException(cause, advert, KEY_NO_RESPONSES_IN_DB);
    }

    public static AdvertException createAdvertTerminatedException(Throwable cause, Advert advert) {
        return createAdvertException(cause, advert, KEY_ADVERT_TERMINATED);
    }

    public static AdvertException createResponsesLimitException(Throwable cause, Advert advert) {
        return createAdvertException(cause, advert, KEY_RESPONSES_LIMIT_EXCEEDED);
    }

    public static AdvertException createTransactionNotExistException(Throwable cause, Advert advert) {
        return createAdvertException(cause, advert, KEY_TRANSTACTION_NOT_EXIST);
    }

    public static AdvertException createNoEntityException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_NO_ENTITY_IN_DB);

        AdvertException advertException = new AdvertException(message);
        return advertException;
    }

    public static AdvertException createNoEntitiesException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_NO_ENTITIES_IN_DB);

        AdvertException advertException = new AdvertException(message);
        return advertException;
    }

    public static AdvertException createUnknownErrorException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_UNKNOWN_ERROR);

        AdvertException advertException = new AdvertException(message);
        return advertException;
    }

    public static AdvertException createWrongDateException() {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(KEY_WRONG_DATE);

        AdvertException advertException = new AdvertException(message);
        return advertException;
    }

    private static AdvertException createAdvertException(Throwable cause, Advert advert, String msg) {
        ResourceBundle bundle = ResourceBundle.getBundle("pl.lodz.p.it.ssbd2016.ssbd02.web.Text", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        String message = bundle.getString(msg);

        AdvertException advertException = new AdvertException(message, cause);
        advertException.setAdvert(advert);
        return advertException;
    }

    private void setAdvert(Advert advert) {
        this.advert = advert;
    }

}
