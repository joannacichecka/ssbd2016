/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mop.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface EmployerFacadeLocal {

    void create(Employer employer) throws ApplicationException;

    void edit(Employer employer) throws ApplicationException;

    void remove(Employer employer);

    Employer find(Object id) throws ApplicationException;

    List<Employer> findAll() throws ApplicationException;

    List<Employer> findRange(int[] range);

    int count();
    
}
