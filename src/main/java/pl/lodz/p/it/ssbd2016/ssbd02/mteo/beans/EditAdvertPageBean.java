package pl.lodz.p.it.ssbd2016.ssbd02.mteo.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.*;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.mteo.endpoints.MTEOEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Piotr Bugara on 06.06.16.
 */

/**
 * Klasa służąca edycji istniejącego ogłoszenia.
 */
@Named("editAdvertPageBean")
@ViewScoped
public class EditAdvertPageBean implements Serializable {

    @EJB
    private MTEOEndpointLocal mteoEndpoint;
    private Advert currentAdvert;
    private Text propertiesDict;
    private String errorMessage;
    private String cityName;
    private Date oldDate;
    private boolean error = false;

    /**
     * Funkcja inicjalizująca, tworząca obiekt typu Text oraz pobiera ogłoszenie do edycji.
     * Ustawia zmienną oldDate, która służy później do sprawdzania, czy nie edytujemy ogłoszenia, które wygasło.
     */
    @PostConstruct
    public void init() {
        propertiesDict = new Text();
        currentAdvert = (Advert) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("advertToEdit");
        oldDate = currentAdvert.getExpiryDate();
    }

    /**
     * Edytuje ogłoszenie.
     * @throws ApplicationException
     */
    public void editAdvert() throws ApplicationException {
        try {
            if(cityName == null)
                cityName = currentAdvert.getCityId().getName();
            mteoEndpoint.editAdvert(currentAdvert, cityName, oldDate);
            addOperationMessage(false, "advertedited");
        } catch (ApplicationException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(CreateAdvertBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }

    /**
     * Dodanie komunikat o wyniku operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Nazwa property, której wartość wyświetlana jest użytkownikowi
     */
    private void addOperationMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, propertiesDict.getString(detail));
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Dodanie komunikatu w przypadku niepowodzenia operacji
     * @param isError Sprawdza, czy wystąpił błąd
     * @param detail Wiadomość ze złapanego wyjątku
     */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String getCityName() {
        return cityName;
    }
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    public String getErrorMessage() {
        return errorMessage;
    }
    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    public boolean isError() {
        return error;
    }
    private void setError(boolean error) {
        this.error = error;
    }
    public Advert getCurrentAdvert() {
        return currentAdvert;
    }
    public void setCurrentAdvert(Advert currentAdvert) {
        this.currentAdvert = currentAdvert;
    }
}
