package pl.lodz.p.it.ssbd2016.ssbd02.mpo.facades;

import javax.ejb.Local;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AdvertResponse;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

@Local
public interface AdvertResponseFacadeLocal {

    /**
     * Tworzy w bazie nowy obiekt AdvertResponse
     * @param advertResponse
     * @throws ApplicationException
     */
    void create(AdvertResponse advertResponse) throws ApplicationException;
}
