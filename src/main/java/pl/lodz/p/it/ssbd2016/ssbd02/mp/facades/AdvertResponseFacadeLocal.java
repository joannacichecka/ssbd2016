/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mp.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.AdvertResponse;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface AdvertResponseFacadeLocal {

    void create(AdvertResponse advertResponse) throws ApplicationException;

    void edit(AdvertResponse advertResponse) throws ApplicationException;

    void remove(AdvertResponse advertResponse);

    AdvertResponse find(Object id) throws ApplicationException;

    List<AdvertResponse> findAll() throws ApplicationException;

    List<AdvertResponse> findRange(int[] range);

    int count();
    
}
