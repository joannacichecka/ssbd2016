/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mteo.facades;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.*;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.Category;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.CategoryException;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Patryk
 */

/**
 * Fasada do operacji na bazie danych dotyczących kategorii ogłoszeń.
 */

@Stateless(name = "CategoryFacadeMTEO")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class CategoryFacade extends AbstractFacade<Category> implements CategoryFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mteoPU")
    private EntityManager em;
    private List<Category> categories;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategoryFacade() {
        super(Category.class);
    }

    @Override
    @RolesAllowed("getAllCategories")
    public List<Category> findAll() throws ApplicationException {
        categories = em.createNamedQuery("Category.findAll").getResultList();
        if(categories.isEmpty()) {
            throw CategoryException.createNoCategoriesInDbException();
        } else {
            return categories;
        }
    }

    @Override
    @RolesAllowed("getCategories")
    public List<Category> findList(List<String> categories) throws ApplicationException {
        List<Category> list = new ArrayList<>();
        if(categories == null)
            return list;
        for(String category : categories) {
            list.add(this.findByName(category));
        }

        if(list.isEmpty()) {
            throw CategoryException.createNoCategoriesInDbException();
        } else {
            return list;
        }
    }

    @Override
    @RolesAllowed("getCategories")
    public Category findByName(String name) throws ApplicationException {
        Query q = em.createNamedQuery("Category.findByName");
        q.setParameter("name", name);
        try {
            return (Category) q.getSingleResult();
        } catch (NoResultException ex) {
            throw CategoryException.createNoCategoriesInDbException();
        } catch (Exception e) {
            throw CategoryException.createUnknownException();
        }
    }
}
