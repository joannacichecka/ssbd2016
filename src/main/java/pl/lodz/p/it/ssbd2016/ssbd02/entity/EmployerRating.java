/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Patryk
 */
@Entity
@Table(name = "employer_rating")
@NamedQueries({
        @NamedQuery(name = "EmployerRating.findAll", query = "SELECT e FROM EmployerRating e"),
        @NamedQuery(name = "EmployerRating.findByComment", query = "SELECT e FROM EmployerRating e WHERE e.comment = :comment"),
        @NamedQuery(name = "EmployerRating.findByRating", query = "SELECT e FROM EmployerRating e WHERE e.rating = :rating"),
        @NamedQuery(name = "EmployerRating.findById", query = "SELECT e FROM EmployerRating e WHERE e.id = :id"),
        @NamedQuery(name = "EmployerRating.findByAccepted", query =
                "SELECT e FROM EmployerRating e WHERE e.accepted = :accepted"),
        @NamedQuery(name = "EmployerRating.findAcceptedByEmployer", query =
                "SELECT e FROM EmployerRating e WHERE e.accepted = true AND e.employerId=:employerId")})
public class EmployerRating implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 350)
    @Column(name = "comment")
    private String comment;
    @Column(name = "rating")
    private Short rating;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "employee_id", referencedColumnName = "id", updatable = false)
    @ManyToOne
    private Employee employeeId;
    @JoinColumn(name = "employer_id", referencedColumnName = "id", updatable = false)
    @ManyToOne
    private Employer employerId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "accepted")
    private boolean accepted;

    public EmployerRating() {
    }

    public EmployerRating(Integer id, boolean accepted) {
        this.id = id;
        this.accepted = accepted;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Short getRating() {
        return rating;
    }

    public void setRating(Short rating) {
        this.rating = rating;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Employee getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employee employeeId) {
        this.employeeId = employeeId;
    }

    public Employer getEmployerId() {
        return employerId;
    }

    public void setEmployerId(Employer employerId) {
        this.employerId = employerId;
    }

    public boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployerRating)) {
            return false;
        }
        EmployerRating other = (EmployerRating) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerRating[ id=" + id + " ]";
    }

}
