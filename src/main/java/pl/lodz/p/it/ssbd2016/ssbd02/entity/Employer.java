/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverter.Lazy;

/**
 * @author Patryk
 */
@Entity
@Table(name = "employer")
@NamedQueries({
        @NamedQuery(name = "Employer.findAll", query = "SELECT e FROM Employer e"),
        @NamedQuery(name = "Employer.findByAccountLogin", query = "SELECT e FROM Employer e WHERE e.account.login = :login"),
        @NamedQuery(name = "Employer.findById", query = "SELECT e FROM Employer e WHERE e.id = :id"),
        @NamedQuery(name = "Employer.findByName", query = "SELECT e FROM Employer e WHERE e.name = :name"),
        @NamedQuery(name = "Employer.findByDescription", query = "SELECT e FROM Employer e WHERE e.description = :description"),
        @NamedQuery(name = "Employer.findByRating", query = "SELECT e FROM Employer e WHERE e.rating = :rating"),
        @NamedQuery(name = "Employer.findByVersion", query = "SELECT e FROM Employer e WHERE e.version = :version"),
        @NamedQuery(name = "Employer.findByProfileConfirmed", query = "SELECT e FROM Employer e WHERE " +
                "e.profileConfirmed = :profileConfirmed")})
public class Employer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    @Size(max = 3000)
    private String description;
    @Max(value = 5)
    @Min(value = 1)
    @Column(name = "rating")
    private Float rating;
    @NotNull
    @Column(name = "version")
    @Version
    private int version;
    @Basic(optional = false)
    @NotNull
    @Column(name = "profile_confirmed")
    private boolean profileConfirmed;
    @JoinTable(name = "employer_category", joinColumns = {
            @JoinColumn(name = "employer_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "category_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Category> categoryList;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "employerId", orphanRemoval = true)
    private List<EmployerLocation> employerLocationList;
    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.REMOVE, CascadeType.PERSIST}, mappedBy = "employerId")
    private List<Advert> advertList;
    @OneToMany(mappedBy = "employerId")
    private List<EmployerRating> employerRatingList;
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Account account;

    public Employer() {
    }

    public Employer(Integer id) {
        this.id = id;
    }

    public Employer(Integer id, String name, int version, boolean profileConfirmed) {
        this.id = id;
        this.name = name;
        this.version = version;
        this.profileConfirmed = profileConfirmed;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean getProfileConfirmed() {
        return profileConfirmed;
    }

    public void setProfileConfirmed(boolean profileConfirmed) {
        this.profileConfirmed = profileConfirmed;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public List<EmployerLocation> getEmployerLocationList() {
        return employerLocationList;
    }

    public void setEmployerLocationList(List<EmployerLocation> employerLocationList) {
        this.employerLocationList = employerLocationList;
    }

    public List<Advert> getAdvertList() {
        return advertList;
    }

    public void setAdvertList(List<Advert> advertList) {
        this.advertList = advertList;
    }

    public List<EmployerRating> getEmployerRatingList() {
        return employerRatingList;
    }

    public void setEmployerRatingList(List<EmployerRating> employerRatingList) {
        this.employerRatingList = employerRatingList;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employer)) {
            return false;
        }
        Employer other = (Employer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer[ id=" + id + " ]";
    }

    public Integer getRatingAsInt() {
        if (rating != null)
            return Math.round(rating);
        return 0;
    }
}
