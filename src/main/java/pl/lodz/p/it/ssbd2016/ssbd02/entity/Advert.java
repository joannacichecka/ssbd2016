/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Patryk
 */
@Entity
@Table(name = "advert")
@NamedQueries({
    @NamedQuery(name = "Advert.findAll", query = "SELECT a FROM Advert a"),
    @NamedQuery(name = "Advert.findById", query = "SELECT a FROM Advert a WHERE a.id = :id"),
    @NamedQuery(name = "Advert.findByEmployer", query = "SELECT a FROM Advert a WHERE a.employerId = :employer"),    
    @NamedQuery(name = "Advert.findByTitle", query = "SELECT a FROM Advert a WHERE a.title = :title"),
    @NamedQuery(name = "Advert.findByDescription", query = "SELECT a FROM Advert a WHERE a.description = :description"),
    @NamedQuery(name = "Advert.findByCreationTime", query = "SELECT a FROM Advert a WHERE a.creationTime = :creationTime"),
    @NamedQuery(name = "Advert.findByResponsesLeft", query = "SELECT a FROM Advert a WHERE a.responsesLeft = :responsesLeft"),
    @NamedQuery(name = "Advert.findByExpiryDate", query = "SELECT a FROM Advert a WHERE a.expiryDate > :expiryDate"),
    @NamedQuery(name = "Advert.findByVersion", query = "SELECT a FROM Advert a WHERE a.version = :version"),
    @NamedQuery(name = "Advert.findByCity", query = "SELECT a FROM Advert a WHERE a.cityId.name = :city"),
    @NamedQuery(name = "Advert.findByCityAndDate", query = "SELECT a FROM Advert a WHERE a.cityId.name = :city " +
            "AND a.expiryDate > :expiryDate"),
    @NamedQuery(name = "Advert.findByQuery", query = "SELECT a FROM Advert a WHERE a.expiryDate > :expiryDate AND " +
            "(lower(a.title) LIKE lower(:phrase) OR lower(a.description) LIKE lower(:phrase) " +
            "OR lower(a.employerId.name) LIKE lower(:phrase) OR a.cityId.name = :city)")})

public class Advert implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "creation_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "responses_left")
    private short responsesLeft;
    @Basic(optional = false)
    @NotNull
    @Column(name = "expiry_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiryDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "version")
    @Version
    private int version;
    @JoinTable(name = "advert_category", joinColumns = {
        @JoinColumn(name = "advert_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "category_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Category> categoryList;
    @JoinColumn(name = "city_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private City cityId;
    @JoinColumn(name = "employer_id", referencedColumnName = "id", updatable = false)
    @ManyToOne(optional = false)
    private Employer employerId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "advertId")
    private List<AdvertResponse> advertResponseList;

    public Advert() {
    }

    public Advert(Integer id) {
        this.id = id;
    }

    public Advert(Integer id, String title, String description, Date creationTime, short responsesLeft, Date expiryDate, int version) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.creationTime = creationTime;
        this.responsesLeft = responsesLeft;
        this.expiryDate = expiryDate;
        this.version = version;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public short getResponsesLeft() {
        return responsesLeft;
    }

    public void setResponsesLeft(short responsesLeft) {
        this.responsesLeft = responsesLeft;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public City getCityId() {
        return cityId;
    }

    public void setCityId(City cityId) {
        this.cityId = cityId;
    }

    public Employer getEmployerId() {
        return employerId;
    }

    public void setEmployerId(Employer employerId) {
        this.employerId = employerId;
    }

    public List<AdvertResponse> getAdvertResponseList() {
        return advertResponseList;
    }

    public void setAdvertResponseList(List<AdvertResponse> advertResponseList) {
        this.advertResponseList = advertResponseList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Advert)) {
            return false;
        }
        Advert other = (Advert) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.ssbd2016.ssbd02.entity.Advert[ id=" + id + " ]";
    }
    
}
