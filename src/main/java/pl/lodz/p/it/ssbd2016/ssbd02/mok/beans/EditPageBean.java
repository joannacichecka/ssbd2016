package pl.lodz.p.it.ssbd2016.ssbd02.mok.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.AccountException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

/**
 * Ziarno widoku edycji własnego konta (dostępna jedynie zmiana adresu e-mail.
 * @author mgrabski
 */

@Named("editPageBean")
@RequestScoped
public class EditPageBean {
	
	@Inject
    private AccountSession accountSession;

	/**
	 * Domyślny konstruktor bezparametrowy inicjalizujący zmiennmą propertiesDict.
	 */
	public EditPageBean() {
        propertiesDict = new Text();
    }

    private Account account = new Account();
    private String errorMessage;
    private Text propertiesDict;
    private boolean error = false;
    
    /**
     * Inicjalizacja modelu - metoda pobiera konto aktualnie zalogowanego użytkownika.
     */
    @PostConstruct
    public void initModel() {
        account = accountSession.getCurrentAccount();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    private void setError(boolean error) {
        this.error = error;
    }
    
    public Account getAccount() {
        return this.account;
    }
    
    /**
     * Metoda umożliwiająca edycję konta użytkownika.
     * @throws AccountException
     */
    public void editAccount() throws AccountException {
        try {
            Account oldAccount = accountSession.getCurrentAccount();
            accountSession.editAccount(oldAccount, this.account);
            addMessage(false, "");
        } catch (ApplicationException ex) {
            setError(true);
            setErrorMessage(ex.getMessage());
            Logger.getLogger(EditPageBean.class.getName()).log(Level.SEVERE, null, ex);
            addMessage(true, ex.getMessage());
        }
    }
    
	/**
	 * Dodanie komunikatu w przypadku niepowodzenia operacji
	 * @param isError Sprawdza, czy wystąpił błąd
	 * @param detail Wiadomość ze złapanego wyjątku
	 */
    private void addMessage(boolean isError, String detail) {
        FacesMessage.Severity severity = isError ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
        String summary = isError ? propertiesDict.getString("operationfailed") :
                propertiesDict.getString("operationsuccess");
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
