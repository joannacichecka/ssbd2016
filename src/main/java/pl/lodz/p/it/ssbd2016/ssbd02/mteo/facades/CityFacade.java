/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mteo.facades;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.*;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.AbstractFacade;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.City;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.services.LoggingInterceptor;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.CityException;

import java.util.List;

/**
 *
 * @author Patryk
 */

/**
 * Fasada do operacji na bazie danych dotyczących miast.
 */
@Stateless(name = "CityFacadeMTEO")
@Interceptors(LoggingInterceptor.class)
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class CityFacade extends AbstractFacade<City> implements CityFacadeLocal {

    @PersistenceContext(unitName = "ssbd02mteoPU")
    private EntityManager em;
    private List<City> cities;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CityFacade() {
        super(City.class);
    }

    @Override
    @RolesAllowed("getCity")
    public City findByName(String name) throws ApplicationException {
        Query q = em.createNamedQuery("City.findByName");
        q.setParameter("name", name);
        try {
            return (City) q.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        } catch (Exception e) {
            throw CityException.createUnknownErrorException();
        }
    }

    @Override
    @RolesAllowed("addCity")
    public void create(City city) throws ApplicationException {
        try {
            em.persist(city);
            em.flush();
        } catch (TransactionRequiredException ex) {
            throw CityException.createTransactionNotExistsException(ex, city);
        } catch(PersistenceException pe) {
            if(pe.getMessage().contains("city_unique")) {
                throw CityException.createNotUniqueException(pe, city);
            }
            else {
                throw CityException.createUnknownErrorException();
            }
        } catch (Exception e) {
            throw CityException.createUnknownErrorException();
        }

    }
}
