/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.ssbd2016.ssbd02.mpo.facades;

import java.util.List;
import javax.ejb.Local;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerRating;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.ApplicationException;

/**
 *
 * @author Patryk
 */
@Local
public interface EmployerRatingFacadeLocal {

    void create(EmployerRating employerRating) throws ApplicationException;

    void edit(EmployerRating employerRating) throws ApplicationException;

    void remove(EmployerRating employerRating);

    EmployerRating find(Object id) throws ApplicationException;

    List<EmployerRating> findAll() throws ApplicationException;

    List<EmployerRating> findRange(int[] range);

    int count();
    
}
