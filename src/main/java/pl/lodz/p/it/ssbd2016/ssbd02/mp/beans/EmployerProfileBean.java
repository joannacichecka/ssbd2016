package pl.lodz.p.it.ssbd2016.ssbd02.mp.beans;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Employer;
import pl.lodz.p.it.ssbd2016.ssbd02.entity.EmployerRating;
import pl.lodz.p.it.ssbd2016.ssbd02.mop.endpoints.MOPEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.mp.endpoints.MPEndpointLocal;
import pl.lodz.p.it.ssbd2016.ssbd02.web.Text;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Bean do wyświetlania konta pracodawcy
 * Created by ljaros on 6/5/16.
 */
@Named("employerProfileBean")
@ViewScoped
public class EmployerProfileBean implements Serializable {
    @EJB
    private MPEndpointLocal mpEndpoint;
    @EJB
    private MOPEndpointLocal mopEndpoint;

    private Employer employer;
    private String errorMessage;
    private boolean error = false;
    private Text propertiesDict;
    private List<EmployerRating> acceptedRatings;

    public List<EmployerRating> getAcceptedRatings() {
        return acceptedRatings;
    }

    public void setAcceptedRatings(List<EmployerRating> acceptedRatings) {
        this.acceptedRatings = acceptedRatings;
    }

    public EmployerProfileBean() {
        this.propertiesDict = new Text();
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public Text getPropertiesDict() {
        return propertiesDict;
    }

    public void setPropertiesDict(Text propertiesDict) {
        this.propertiesDict = propertiesDict;
    }

    /**
     * Inicjalizuje konto pracodawcy
     */
    @PostConstruct
    private void initModel(){
        employer = (Employer) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("employerToShow");
        populateAcceptedRatings();
    }

    private void populateAcceptedRatings(){
        acceptedRatings = new ArrayList<>();
        for(EmployerRating rating : employer.getEmployerRatingList()){
            if(rating.getAccepted())
                acceptedRatings.add(rating);
        }
    }
    public String setEmployerToRate(){
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("employerToRate", employer);
        return "rating";
    }

}
