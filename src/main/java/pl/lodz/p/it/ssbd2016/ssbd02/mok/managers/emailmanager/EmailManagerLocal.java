package pl.lodz.p.it.ssbd2016.ssbd02.mok.managers.emailmanager;

import pl.lodz.p.it.ssbd2016.ssbd02.entity.Account;
import pl.lodz.p.it.ssbd2016.ssbd02.exceptions.EmailException;

import javax.ejb.Local;

/**
 * Created by Joanna Cichecka on 2016-06-16.
 */
@Local
public interface EmailManagerLocal {

    /**
     * Metoda do wysyłania wiadomości e-mail.
     * @param account konto osoby, która ma otrzymać wiadomość
     * @param emailType typ wiadomości
     * @throws EmailException
     */
    void sendEmail(Account account, EmailGenerator.EmailType emailType) throws EmailException;
}
